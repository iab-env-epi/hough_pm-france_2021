#!/usr/bin/env bash

#OAR --project pr-edenpelagie
#OAR -l /nodes=1,walltime=00:30:00

set -e

# Activate conda environment
source ~/init-mamba.sh
micromamba activate r4.4.3

# Compute area-weighted daily PM for each IRIS
Rscript extract-by-iris.R

# Return to current dir on exit
trap "cd \"${PWD}\"" EXIT

# Create annual tar archives of IRIS data
cd work/preds
for year in $(seq 2000 2019);
do
  csv_pattern="iris-20[0-9][0-9]_pm_${year}-[01][0-9].csv"
  iris_year_pattern="(?<=iris-)\d{4}(?=_)"
  iris_year=$(ls $csv_pattern | head -n 1 ${csvs[1]} | grep -oP $iris_year_pattern)
  tar_path="iris_pm_${year}.tar.gz"
  if [[ ! -f $tar_path ]]; then
    echo "Archiving ${year} IRIS PM"
    tar --create --gzip --file "${tar_path}.tmp" $csv_pattern
    mv "${tar_path}.tmp" "$tar_path"
    echo "-> work/preds/${tar_path}"
  fi
done
