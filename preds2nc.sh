#!/usr/bin/env bash

#OAR --project pr-edenpelagie
#OAR -l /nodes=1/core=12,walltime=02:00:00

set -e

# Activate conda environment
source ~/init-mamba.sh
micromamba activate r4.4.3

# Write predictions to netCDFs
Rscript preds2nc.R

# Return to current dir on exit
trap "cd \"${PWD}\"" EXIT

# Create annual tar archives of netCDFs
cd work/preds
for year in $(seq 2000 2019);
do
  tar_path="pm_1km_${year}_nc.tar"
  if [[ ! -f $tar_path ]]; then
    echo "Archiving ${year} netCDFs"
    tar --create --file "${tar_path}.tmp" "pm_1km_${year}*.nc"
    mv "${tar_path}.tmp" "$tar_path"
    echo "-> work/preds/${tar_path}"
  fi
done
