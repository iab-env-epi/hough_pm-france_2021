#!/usr/bin/env bash

# Sync final PM predictions to summer/modeles_expo/pm_hough_1km
# Use --size-only b/c times cannot be set on summer and checksum is *very* slow
dst_dir=/media/summer_iab/modeles_expo/pm_hough_1km
[ ! -d $dst_dir ] && mkdir $dst_dir
rsync -iP --size-only ./work/preds/pm_1km_20[0-9][0-9].fst $dst_dir  # 1 km predictions
rsync -iP --size-only ./work/geo/grd_1km_p[ty]s.gpkg $dst_dir  # 1 km grid centroids + polygons

# Sync monthly summary statistics (per-grid-cell min / mean / max)
dst_dir=/media/summer_iab/modeles_expo/pm_hough_1km/stats
[ ! -d $dst_dir ] && mkdir $dst_dir
rsync -iP --size-only ./work/preds/pm_1km_*_smry.fst $dst_dir  # 1 km summary statistics
