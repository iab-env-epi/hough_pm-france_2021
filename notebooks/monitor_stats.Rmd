---
title: "Monitor PM descriptive statistics"
output:
  html_notebook:
    code_folding: "hide"
editor_options: 
  chunk_output_type: inline
---

```{r setup}
source(here::here("init.R"))
knitr::opts_chunk$set(fig.height = 6, fig.width = 9.7)

```

```{r load-data}
# Load data
db1 <- read_fst(here("work/db/db1_2000-2019.fst"), as.data.table = TRUE) %>% 
       melt(id.vars = c("stn_code", "date", "impl", "infl"), measure.vars = c("PM2.5", "PM10"),
            variable.name = "pol") %>% 
       na.omit()

```


## PM levels over time

```{r}
db1[, as.list(quantile(value, c(0.25, 0.5, 0.75))),
    keyby = .(pol, time = floor_date(date, "year"))] %>%
  ggplot(aes(x = time, fill = pol)) +
    geom_ribbon(aes(ymin = `25%`, ymax = `75%`), alpha = 0.5, size = 0, show.legend = FALSE) +
    geom_line(aes(y = `50%`)) +
    facet_grid(vars(pol), scales = "free_y") +
    scale_x_date(date_breaks = "year", date_labels = "%Y") +
    labs(x = element_blank(), y = element_blank(), title = "Monitor PM over time",
         subtitle = "Line shows median; shaded area shows Q1 - Q3 interval (25% - 75%)") +
    theme_bw()

```


## Distribution of monitor PM over time

```{r}
set.seed(100)
outlier_level <- 0.99
db1[, outlier := value > quantile(value, outlier_level), by = .(pol, year(date))]
db1[outlier == FALSE, ] %>% 
  ggplot(aes(x = value, y = factor(year(date)), fill = pol)) +
    geom_violin(draw_quantiles = 0.5 / outlier_level, show.legend = FALSE) + # draw true median
    geom_point(data = db1[outlier == TRUE, ], position = "jitter", alpha = 0.4,
               show.legend = FALSE) +
    facet_grid(col = vars(pol), scales = "free_x", space = "free_x") +
    scale_x_continuous(n.breaks = 10) +
    scale_y_discrete(limits = rev) +
    labs(x = element_blank(), y = element_blank(), title = "Monitor PM distribution by year",
         subtitle = "Line shows median; points show highest 1% of observations") +
    theme_bw()
rm(outlier_level)

```
