source("init.R")

pol <- "PM10"
year <- 2012
ncores <- NCORES
force <- F
verbose <- T

src_dir <- here(glue("work/models/{year}"))

msg_prefix <- glue("[fit_m4] {pol} {year}:")


# Setup -----

fold = 3
cv_prefix <- glue("{msg_prefix} CV {fold}:")

# Load all test set (out-of-sample) m3 predictions from each model for the CV fold
# -> list of 3 data.tables, each with column identifying CV ID and model
report(glue("{cv_prefix} loading m3 CV test set predictions"))
models <- c("lmm", "gmrf", "rf") %>%
          factor(., levels = .)
dat <- purrr::map(models, function(m) {

         dt <- glue("m3_{tolower(pol)}_{year}_{m}_.*{fold}.*_cv.fst") %>%
               list.files(src_dir, full.names = TRUE, pattern = .) %>%
               purrr::map(function(p) {

                 # Only keep test set predictions
                 dt <- read_fst(p, as.data.table = TRUE) %>%
                       .[cv_set == "test", ] %>%
                       .[, cv_set := NULL]

                 # Label with CV ID and model
                 cv_id <- basename(p) %>%
                          str_extract(glue("(?<={m}_).*{fold}.*(?=_cv)"))
                 dt[, cv_id := ..cv_id]
                 dt[, model := ..m]
                 dt

               }) %>%
               rbindlist()
         dt

       })
dat %<>% rbindlist()
names(dat) %>% paste(collapse = " ")
# date sinu_id sinu_x sinu_y cv_fold stn_code impl infl PM10 pred_m3 cv_id model

# Cast long to wide
# -> 1 row per day - grid cell - cv id; 1 column for PM and each model's prediction
dat %<>% dcast(... ~ model, value.var = "pred_m3") %>%
         setcolorder(c("date", "sinu_id", "sinu_x", "sinu_y", "stn_code", "impl", "infl",
                       "cv_fold", "cv_id", pol))
names(dat) %>% paste(collapse = " ")
# date sinu_id sinu_x sinu_y stn_code impl infl cv_fold cv_id PM10 lmm gmrf rf

# Add time index (GAM can't seem to handle dates)
dat[, t_idx := yday(date)]

# Identify train and test sets
cv_sets <- list(
             test = dat[cv_fold == fold, which = TRUE],
             train = dat[cv_fold != fold, which = TRUE]
           )



# Initial exploration -----

# Helper function
assess <- function(m) {

  # Predict for the train and test sets
  dat[, pred_m4 := predict(m, newdata = dat)]

  # Report performance
  dat[, assess_preds(.SD, ..pol, "pred_m4", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()

}


# Misc exploration
# * little benefit from cluster or nthreads [1c = 69 sec; 2c = 55 sec; 24c = 60 sec]
#                                          rmse     r2   mae
# BAM plain: 41 sec fit / 33 sec predict  5.136  0.858 3.582
# BAM clust: 44 sec fit / 30 sec predict  5.086  0.862 3.571


# Which is better? GAM / BAM and s(tp) / te(tp) / te(cr)
# * BAM is faster than GAM; equivalent performance
# * s(tp) is better but slower than te(cr);
# * te(cr) is faster than te(tp); equivalent performance
#                          seconds  rmse     r2   mae
# GAM s(k = 125, bs = tp): 50 / 50  5.14  0.858  3.59 [k too low]
# GAM te(k = 5, bs = tp):  17 /  5  5.28  0.850  3.71 [k too low]
# GAM te(k = 5, bs = cr):  17 /  5  5.28  0.850  3.71 [k too low]
#
# BAM s(k = 125, bs = tp): 35 / 30  5.14  0.858  3.58 [k too low]
# BAM te(k = 5, bs = tp):  13 /  7  5.28  0.850  3.72 [k too low]
# BAM te(k = 5, bs = cr):   8 /  3  5.28  0.850  3.72 [k too low]
{
  report("GAM s()")
  start <- Sys.time()
  f_gam_s <- glue("{pol} ~ s(sinu_x, sinu_y, t_idx, by = lmm, bs = 'tp', k = 125) +
                           s(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 125) +
                           s(sinu_x, sinu_y, t_idx, by = rf, bs = 'tp', k = 125)")
  report(f_gam_s)
  m_gam_s <- f_gam_s %>%
             as.formula(env = new.env()) %>%
             mgcv::gam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_gam_s)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_gam_s))
  report("")

  report("GAM te(bs = tp)")
  start <- Sys.time()
  f_gam_te_tp <- glue("{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm, bs = 'tp', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = rf, bs = 'tp', k = 5)")
  report(f_gam_te_tp)
  m_gam_te_tp <- f_gam_te_tp %>%
                 as.formula(env = new.env()) %>%
                 mgcv::gam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_gam_te_tp)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_gam_te_tp))
  report("")

  report("GAM te(bs = cr)")
  start <- Sys.time()
  f_gam_te_cr <- glue("{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm, bs = 'cr', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'cr', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = rf, bs = 'cr', k = 5)")
  report(f_gam_te_cr)
  m_gam_te_cr <- f_gam_te_cr %>%
                 as.formula(env = new.env()) %>%
                 mgcv::gam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_gam_te_cr)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_gam_te_cr))
  report("")

  report("BAM s()")
  start <- Sys.time()
  f_bam_s <- glue("{pol} ~ s(sinu_x, sinu_y, t_idx, by = lmm, bs = 'tp', k = 125) +
                           s(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 125) +
                           s(sinu_x, sinu_y, t_idx, by = rf, bs = 'tp', k = 125)")
  report(f_bam_s)
  m_bam_s <- f_bam_s %>%
             as.formula(env = new.env()) %>%
             mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_s)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_s))
  report("")

  report("BAM te(bs = tp)")
  start <- Sys.time()
  f_bam_te_tp <- glue("{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm, bs = 'tp', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = rf, bs = 'tp', k = 5)")
  report(f_bam_te_tp)
  m_bam_te_tp <- f_bam_te_tp %>%
                 as.formula(env = new.env()) %>%
                 mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_te_tp)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_te_tp))
  report("")

  report("BAM te(bs = cr)")
  start <- Sys.time()
  f_bam_te_cr <- glue("{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm, bs = 'cr', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'cr', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = rf, bs = 'cr', k = 5)")
  report(f_bam_te_cr)
  m_bam_te_cr <- f_bam_te_cr %>%
                 as.formula(env = new.env()) %>%
                 mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_te_cr)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_te_cr))
  report("")
}


# Does increasing k improve performance?
# * Yes, but diminishing returns for s() beyond k ~= 725
# * s() outperforms te()
#                            minutes  rmse     r2   mae
# BAM s(k = 512, bs = tp): 1.5 / 0.8  4.57  0.888  3.16 [k too low]
# BAM s(k = 729, bs = tp): 3.0 / 0.9  4.52  0.890  3.12 [k too low]
# BAM s(k = 900, bs = tp): 4.5 / 1.0  4.51  0.891  3.11 [k too low]
# BAM s(k = 1e3, bs = tp): Error in Rrank(qr.R(qrx), tol = .Machine$double.eps^0.75) : NA/NaN/Inf in foreign function call (arg 1)
#
# BAM te(k = 7, bs = tp):  0.5 / 0.1  5.21  0.854  3.66 [k too low]
# BAM te(k = 8, bs = tp):  1.1 / 0.2  5.18  0.855  3.63 [k too low]
# BAM te(k = 9, bs = tp):  2.0 / 0.2  5.13  0.858  3.59 [k too low]
# BAM te(k = 10, bs = tp): 4.3 / 0.3  5.09  0.860  3.56 [k too low]
# BAM te(k = 11, bs = tp): 11.6 / 0.3 5.04  0.863  3.52 [k too low]
#
# BAM te(k = 7, bs = cr):  0.4 / 0.1  5.21  0.854  3.65 [k too low]
# BAM te(k = 8, bs = cr):  1.3 / 0.1  5.17  0.856  3.62 [k too low]
# BAM te(k = 9, bs = cr):  1.7 / 0.1  5.14  0.858  3.59 [k too low]
# BAM te(k = 10, bs = cr): 4.2 / 0.1  5.07  0.861  3.55 [k too low]
{
  report("BAM s()")
  start <- Sys.time()
  f_bam_s <- glue("{pol} ~ s(sinu_x, sinu_y, t_idx, by = lmm, bs = 'tp', k = 512) +
                           s(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 512) +
                           s(sinu_x, sinu_y, t_idx, by = rf, bs = 'tp', k = 512)")
  report(f_bam_s)
  m_bam_s <- f_bam_s %>%
             as.formula(env = new.env()) %>%
             mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_s)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_s))
  report("")

  report("BAM te(bs = tp)")
  start <- Sys.time()
  f_bam_te_tp <- glue("{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm, bs = 'tp', k = 8) +
                               te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 8) +
                               te(sinu_x, sinu_y, t_idx, by = rf, bs = 'tp', k = 8)")
  report(f_bam_te_tp)
  m_bam_te_tp <- f_bam_te_tp %>%
                 as.formula(env = new.env()) %>%
                 mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_te_tp)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_te_tp))
  report("")

  report("BAM te(bs = cr)")
  start <- Sys.time()
  f_bam_te_cr <- glue("{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm, bs = 'cr', k = 5) +
                               te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'cr', k = 7) +
                               te(sinu_x, sinu_y, t_idx, by = rf, bs = 'cr', k = 9)")
  report(f_bam_te_cr)
  m_bam_te_cr <- f_bam_te_cr %>%
                 as.formula(env = new.env()) %>%
                 mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_te_cr)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_te_cr))
  report("")
}

# Does 'by = t_idx' work better than 'by = [model]'?
# * 'by = [model]' is best
#                                        minutes  rmse     r2   mae
# BAM s(by = t_idx, k = 512, bs = tp): 2.0 / 2.2  8.08  0.648  5.69 [k bizarre]
# BAM te(by = t_idx, k = 8, bs = tp):  4.6 / 1.8  7.83  0.670  5.55 [k too low; unstable?]


# Does a 4D smooth work beter than 'by = [model]'?
# * 'by = [model]' is best
#                                minutes  rmse     r2   mae
# BAM s(4D, k = 625, bs = tp): 4.5 / 3.6  4.64  0.884  3.20
# BAM te(4D, bs = cr):         2.4 / 0.2  5.22  0.853  3.67 [k 624 / 487 / 483 marginal]
# BAM te(4D, k = 5, bs = tp): Error in `[<-`(`*tmp*`, , object$smooth[[k]]$first.para:object$smooth[[k]]$last.para,  : subscript out of bounds


# Do separate smooths for space + time work better than a 3D smooth?
# * For s(), 3D smooth is superior
# * For te(), separate smooths are better than 3D but still slightly worse than s(3D)
#                                                         minutes  rmse    r2  mae
# BAM s(xy, k = 325) + s(t, k = 50):                    0.8 / 0.6  4.63 0.885 3.21 [k ok?]
# BAM te(xy, bs = tp, k = 25) + te(t, bs = tp, k = 50): 2.6 / 0.3  4.61 0.886 3.19 [k ok?]
# BAM te(xy, bs = cr, k = 25) + te(t, bs = cr, k = 50): 1.7 / 0.2  4.61 0.886 3.19 [k ok?]
{
  report("BAM s(space) + s(time)")
  start <- Sys.time()
  f_bam_s <- glue("{pol} ~ s(sinu_x, sinu_y, by = lmm, bs = 'tp', k = 325) +
                           s(sinu_x, sinu_y, by = gmrf, bs = 'tp', k = 325) +
                           s(sinu_x, sinu_y, by = rf, bs = 'tp', k = 325) +
                           s(t_idx, by = lmm, bs = 'tp', k = 50) +
                           s(t_idx, by = gmrf, bs = 'tp', k = 50) +
                           s(t_idx, by = rf, bs = 'tp', k = 50)")
  report(f_bam_s)
  m_bam_s <- f_bam_s %>%
             as.formula(env = new.env()) %>%
             mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_s)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_s))
  report("")

  report("BAM te(space, bs = tp) + te(time, bs = tp)")
  start <- Sys.time()
  f_bam_te_tp <- glue("{pol} ~ te(sinu_x, sinu_y, by = lmm, bs = 'tp', k = 25) +
                               te(sinu_x, sinu_y, by = gmrf, bs = 'tp', k = 25) +
                               te(sinu_x, sinu_y, by = rf, bs = 'tp', k = 25) +
                               te(t_idx, by = lmm, bs = 'tp', k = 50) +
                               te(t_idx, by = gmrf, bs = 'tp', k = 50) +
                               te(t_idx, by = rf, bs = 'tp', k = 50)")
  report(f_bam_te_tp)
  m_bam_te_tp <- f_bam_te_tp %>%
                 as.formula(env = new.env()) %>%
                 mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_te_tp)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_te_tp))
  report("")

  report("BAM te(space, bs = cr) + te(time, bs = cr)")
  start <- Sys.time()
  f_bam_te_cr <- glue("{pol} ~ te(sinu_x, sinu_y, by = lmm, bs = 'cr', k = 25) +
                               te(sinu_x, sinu_y, by = gmrf, bs = 'cr', k = 25) +
                               te(sinu_x, sinu_y, by = rf, bs = 'cr', k = 25) +
                               te(t_idx, by = lmm, bs = 'cr', k = 50) +
                               te(t_idx, by = gmrf, bs = 'cr', k = 50) +
                               te(t_idx, by = rf, bs = 'cr', k = 50)")
  report(f_bam_te_cr)
  m_bam_te_cr <- f_bam_te_cr %>%
                 as.formula(env = new.env()) %>%
                 mgcv::bam(data = dat[cv_sets$train, ])
  report(format(Sys.time() - start))
  assess(m_bam_te_cr)
  report(format(Sys.time() - start))
  print(mgcv::k.check(m_bam_te_cr))
  report("")
}



# Further exploration -----

# Helper functions
pred <- function(m) {

  report(glue("AIC: {round(m$aic, 3)}  fREML: {round(summary(m)$sp.criterion, 3)}"))

  colname <- deparse(substitute(m))
  dat[, (colname) := predict(m, newdata = dat)]

}

assess <- function(colname) {

  # Report performance
  dat[cv_sets$test, cv_set := factor("test", levels = c("test", "train"))]
  dat[cv_sets$train, cv_set := factor("train", levels = c("test", "train"))]
  dat[, assess_preds(.SD, ..pol, ..colname, "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()

}


# * s() is better than te() for spatial smooth
#   - s(tp) and s(ts) are equivalent
#   - te() can be improved by increasing k for rf smooth
# * spatial + temporal smooth is better than spatial alone
#   - s(tp) and s(ts) are equivalent


# Best seems to be mixed: te(spacetime [LMM, GMRF]) + s(space [RF]) + s(time [RF])
# * s(spacetime [LMM, GMRF]) may have slightly lower RMSE but unclear if difference is robust:
#   for some folds s() has negative AIC and negative effective effective degrees of freedom for
#   the GMRF spacetime smooth, suggesting something strange is happening. Using s() also implicitly
#   assumes that space + time have same scale i.e. 1 meter is equivalent to 1 day
# * te(spacetime [LMM, RF]) seems equivalent to te(spacetime [LMM, GMRF])
# * using bs = 'cs' or bs = 'tp' for te(spacetime) does not improve performance
# * s(spacetime) for all seems to overfit RF
# * s(space) + s(time) for all is worse than te(spacetime [LMM, GMRF])
# * s(km_x, km_y, time) for all avoids overfitting but worse than s(space) + s(time) for all
# * te(spacetime) for all is worst of all
{
  # Spatial + temporal s for all models
  report(glue("{cv_prefix} s(space, bs = tp) + s(time, bs = tp)"))
  start <- Sys.time()
  m_s_space_time <- "{pol} ~ s(sinu_x, sinu_y, by = lmm,  bs = 'tp', k = 250) +
                             s(sinu_x, sinu_y, by = gmrf, bs = 'tp', k = 275) +
                             s(sinu_x, sinu_y, by = rf,   bs = 'tp', k = 345) +
                             s(t_idx,          by = lmm,  bs = 'tp', k =  50) +
                             s(t_idx,          by = gmrf, bs = 'tp', k = 100) +
                             s(t_idx,          by = rf,   bs = 'tp', k = 200)" %>%
                  glue() %>%
                  as.formula(env = new.env()) %>%
                  bam(data = dat[cv_sets$train, ])
  print(mgcv::k.check(m_s_space_time))
  report(Sys.time() - start)
  pred(m_s_space_time)
  report(Sys.time() - start)
  assess("m_s_space_time")
  print("")

  # Spatiotemporal te for LMM and GMRF, spatial + temporal s for RF
  report(glue("{cv_prefix} te(spacetime [LMM, GMRF]) + s(space [RF]) + s(time [RF])"))
  start <- Sys.time()
  m_te_s_mixed <- "{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm,  bs = 'cr', k = 7) +
                           te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'cr', k = 9) +
                           s(sinu_x, sinu_y,         by = rf,   bs = 'tp', k = 345) +
                           s(t_idx,                  by = rf,   bs = 'tp', k = 200)" %>%
             glue() %>%
             as.formula(env = new.env()) %>%
             bam(data = dat[cv_sets$train, ])
  print(mgcv::k.check(m_te_s_mixed))
  report(Sys.time() - start)
  pred(m_te_s_mixed)
  report(Sys.time() - start)
  assess("m_te_s_mixed")
  print("")

  # Spatiotemporal s for LMM and GMRF, spatial + temporal s for RF
  report(glue("{cv_prefix} s(spacetime [LMM, GMRF]) + s(space [RF]) + s(time [RF])"))
  start <- Sys.time()
  m_s_mixed <- "{pol} ~ s(sinu_x, sinu_y, t_idx,  by = lmm,  bs = 'tp', k = 350) +
                        s(sinu_x, sinu_y, t_idx,  by = gmrf, bs = 'tp', k = 750) +
                        s(sinu_x, sinu_y,         by = rf,   bs = 'tp', k = 345) +
                        s(t_idx,                  by = rf,   bs = 'tp', k = 200)" %>%
             glue() %>%
             as.formula(env = new.env()) %>%
             bam(data = dat[cv_sets$train, ])
  print(mgcv::k.check(m_s_mixed))
  report(Sys.time() - start)
  pred(m_s_mixed)
  report(Sys.time() - start)
  assess("m_s_mixed")
  print("")

  # Spatiotemporal te for all models
  report(glue("{cv_prefix} te(spacetime [LMM, GMRF, RF])"))
  start <- Sys.time()
  m_te_spacetime <- "{pol} ~ te(sinu_x, sinu_y, t_idx, by = lmm,  bs = 'cr', k = 7) +
                             te(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'cr', k = 9) +
                             te(sinu_x, sinu_y, t_idx, by = rf,   bs = 'cr', k = 9)" %>%
                  glue() %>%
                  as.formula(env = new.env()) %>%
                  bam(data = dat[cv_sets$train, ])
  print(mgcv::k.check(m_te_spacetime))
  report(Sys.time() - start)
  pred(m_te_spacetime)
  report(Sys.time() - start)
  assess("m_te_spacetime")
  print("")

  # Spatiotemporal s for all models
  report(glue("{cv_prefix} s(spacetime [LMM, GMRF, RF])"))
  start <- Sys.time()
  m_s_spacetime <- "{pol} ~ s(sinu_x, sinu_y, t_idx, by = lmm,  bs = 'tp', k = 300) +
                            s(sinu_x, sinu_y, t_idx, by = gmrf, bs = 'tp', k = 375) +
                            s(sinu_x, sinu_y, t_idx, by = rf,   bs = 'tp', k = 725)" %>%
                  glue() %>%
                  as.formula(env = new.env()) %>%
                  bam(data = dat[cv_sets$train, ])
  print(mgcv::k.check(m_s_spacetime))
  report(Sys.time() - start)
  pred(m_s_spacetime)
  report(Sys.time() - start)
  assess("m_s_spacetime")
  print("")

}
# 2020-07-15 10:29:03: [fit_m4] PM10 2012: CV 3: s(space, bs = tp) + s(time, bs = tp)
#                        k'        edf  k-index p-value
# s(sinu_x,sinu_y):lmm  250 172.066941 1.002282  0.6250
# s(sinu_x,sinu_y):gmrf 275 190.493726 1.002282  0.5525
# s(sinu_x,sinu_y):rf   345 336.626816 1.002282  0.5250
# s(t_idx):lmm           50   2.004295 1.008761  0.7800
# s(t_idx):gmrf         100  56.937753 1.008761  0.7275
# s(t_idx):rf           200 126.147050 1.008761  0.7425
# 2020-07-15 10:30:06: 1.0518190741539
# 2020-07-15 10:30:06: AIC: 551419.747  fREML: 277649.68
# 2020-07-15 10:30:42: 1.65362703800201
# 2020-07-15 10:30:42: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
# 2020-07-15 10:30:42:   test 4.5159 0.8907 3.1631     1.0477   0.9677    4.4571  0.8723  -34.069 54.8651 94180  23.1049 13.6561
# 2020-07-15 10:30:42:  train 4.4775 0.8929 3.1292     0.9546   0.9709    4.4578   0.873 -31.1292 73.3067 94182  23.0748 13.6811
#
# 2020-07-15 10:30:42: [fit_m4] PM10 2012: CV 3: te(spacetime [LMM, GMRF]) + s(space [RF]) + s(time [RF])
#                               k'      edf   k-index p-value
# te(sinu_x,sinu_y,t_idx):lmm  343 151.9349 0.9729316  0.0200
# te(sinu_x,sinu_y,t_idx):gmrf 729 437.1970 0.9751471  0.0525
# s(sinu_x,sinu_y):rf          345 340.5243 0.9872357  0.1850
# s(t_idx):rf                  200 122.9658 1.0074008  0.7250
# 2020-07-15 10:31:44: 1.03520337343216
# 2020-07-15 10:31:45: AIC: 550105.971  fREML: 277155.807
# 2020-07-15 10:32:03: 1.33680889606476
# 2020-07-15 10:32:03: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
# 2020-07-15 10:32:03:   test 4.4792 0.8924 3.1348     0.9831   0.9716    4.4223  0.8742 -31.4701 58.3245 94180  23.1049 13.6561
# 2020-07-15 10:32:03:  train 4.4384 0.8948    3.1     0.8548   0.9766    4.4205  0.8751 -29.7143 74.4202 94182  23.0748 13.6811
#
# 2020-07-15 10:32:03: [fit_m4] PM10 2012: CV 3: s(spacetime [LMM, GMRF]) + s(space [RF]) + s(time [RF])
#                              k'        edf   k-index p-value
# s(sinu_x,sinu_y,t_idx):lmm  350   155.0695 0.9342276   0.000
# s(sinu_x,sinu_y,t_idx):gmrf 750 -1946.9037 0.9342276   0.000
# s(sinu_x,sinu_y):rf         345   240.5328 0.9938329   0.310
# s(t_idx):rf                 200   115.6040 0.9970548   0.395
# 2020-07-15 10:33:25: 1.37227940956752
# 2020-07-15 10:33:26: AIC: 542201.072  fREML: 276634.793
# 2020-07-15 10:34:07: 2.07077933152517
# 2020-07-15 10:34:08: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
# 2020-07-15 10:34:08:   test 4.4273 0.8949 3.0926     0.9206   0.9751    4.3775  0.8768 -34.6259  57.185 94180  23.1049 13.6561
# 2020-07-15 10:34:08:  train   4.37  0.898 3.0502     0.8522   0.9768    4.3602  0.8785 -30.0559 72.7633 94182  23.0748 13.6811
#
# 2020-07-15 10:34:08: [fit_m4] PM10 2012: CV 3: te(spacetime [LMM, GMRF, RF])
#                               k'      edf   k-index p-value
# te(sinu_x,sinu_y,t_idx):lmm  343 137.4630 0.9603882  0.0025
# te(sinu_x,sinu_y,t_idx):gmrf 729 340.4736 0.9593119  0.0000
# te(sinu_x,sinu_y,t_idx):rf   729 323.2565 0.9596137  0.0000
# 2020-07-15 10:35:12: 1.07749238411585
# 2020-07-15 10:35:13: AIC: 574804.539  fREML: 288570.934
# 2020-07-15 10:35:19: 1.19009292125702
# 2020-07-15 10:35:20: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
# 2020-07-15 10:35:20:   test 5.0594 0.8627 3.5693      2.186   0.8594    4.6018  0.8638  -34.787 62.7346 94180  23.1049 13.6561
# 2020-07-15 10:35:20:  train 5.0738 0.8625 3.5643      2.206   0.8443    4.6194  0.8636 -32.2552 72.5128 94182  23.0748 13.6811
#
# 2020-07-15 10:35:20: [fit_m4] PM10 2012: CV 3: s(spacetime [LMM, GMRF, RF])
#                              k'          edf   k-index p-value
# s(sinu_x,sinu_y,t_idx):lmm  300     148.7132 0.9031697       0
# s(sinu_x,sinu_y,t_idx):gmrf 375     165.4276 0.9031697       0
# s(sinu_x,sinu_y,t_idx):rf   725 -642806.6560 0.9031697       0
# 2020-07-15 10:36:27: 1.12198839187622
# 2020-07-15 10:36:27: AIC: -738216.134  fREML: 277044.267
# 2020-07-15 10:37:05: 1.76148029168447
# 2020-07-15 10:37:06: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
# 2020-07-15 10:37:06:   test 4.4603 0.8933 3.1178     1.1286   0.9625    4.4077  0.8751 -34.3157  57.314 94180  23.1049 13.6561
# 2020-07-15 10:37:06:  train 4.4095 0.8961 3.0738     1.0785   0.9628    4.3973  0.8764 -30.9958 73.8282 94182  23.0748 13.6811
