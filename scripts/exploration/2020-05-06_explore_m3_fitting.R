VERBOSE <- TRUE
source("init.R")

# Functions -----

# Project a GMRF and convert to raster
inla_field_to_raster <- function(field, projector) {

  # Project the field
  pj <- inla.mesh.project(projector, field)

  # Convert to raster with proper CRS and coordinates
  if (is.matrix(pj)) {

    # Projector uses x and y coordinates:
    # pj is matrix; top left = xmin,ymin; bottom right = xmax;ymax

    # Rotate matrix to raster format
    # -> matrix top left = xmin,ymax; bottom right = xmax,ymin
    pj <- t(pj)[rev(seq_len(ncol(pj))), ]

    # Get x and y coordinate ranges from projector
    xrng <- range(projector$x)
    yrng <- range(projector$y)

    r <- raster(pj, crs = projector$crs, xmn = xrng[1], xmx = xrng[2], ymn = yrng[1], ymx = yrng[2])

  } else {

    # Projector uses locations:
    # pj is vector of length nrow(projector$loc)

    # Convert to raster with proper CRS and coordinates
    pj <- cbind(projector$loc, layer = pj)
    r <- grid_to_raster(pj, coords = colnames(projector$loc), crs = projector$crs)$layer

  }

}

# Calculate a random effect of an INLA model on a grid
inla_ranef_on_grid <- function(dt, mesh, field, field_groups) {

  # Create a projector matrix that links each grid cell to the mesh
  locs <- dt[, .(km_x = first(sinu_x) / 1000, km_y = first(sinu_y) / 1000), keyby = .(sinu_id)]
  proj <- inla.mesh.projector(
            mesh = mesh,
            loc = locs[, .(km_x, km_y)] %>%
                  as.matrix(),
            crs = mesh$crs
          )

  # Determine which time levels we need from the random field
  times <- dt[, unique(t_idx)]
  field_idx <- which(field_groups %in% times)
  field_groups <- field_groups[field_idx]

  # Calculate the random effect for each cell-day
  ranef <- field[field_idx] %>%
           split(field_groups) %>%
           purrr::map(function(x) {
             cbind(locs, ranef = inla.mesh.project(proj, x))
           }) %>%
           rbindlist
  ranef[, t_idx := rep(times, each = nrow(locs))]
  ranef <- ranef[dt, ranef, on = .(t_idx, sinu_id)]

  return(ranef)

}

# Calculate the fixed effect of an INLA model on a grid with covariates
inla_fixef_on_grid <- function(dt, inla) {

  # Extract the mean coefficients
  beta <- inla$summary.fixed$mean %>%
          purrr::set_names(inla$names.fixed)

  # Multiply by covariates
  fixef <- dt[, inla$names.fixed, with = FALSE] %>%
           { as.matrix(.) %*% diag(beta) } %>%
           Matrix::rowSums()

  return(fixef)

}

# Naive prediction on a grid
# Assumes model has a single spatiotemporal random intecept effect named "space"
inla_predict_on_grid <- function(dt, inla, mesh, field_groups, trafo = NULL) {

  if (!identical(names(inla$summary.random), "space")) {
    stop("This method only works for models with a single random intercept effect named 'space'")
  }

  # Combine fixed and random effects
  pred <- inla_ranef_on_grid(dt, mesh, inla$summary.random$space$mean, field_groups) +
          inla_fixef_on_grid(dt, inla)

  # Possibly transform
  if (is.function(trafo)) {
    pred <- trafo(pred)
  }

  return(pred)

}

# Convert an INLA mesh to a sf object
mesh_to_sf <- function(mesh) {

  # List the endpoints of every edge
  # * x1, y1, x2, y2, edge id
  df <- rbind(
          data.frame(
            a = mesh$loc[mesh$graph$tv[, 1], c(1, 2)],
            b = mesh$loc[mesh$graph$tv[, 2], c(1, 2)],
            id = paste0("a", seq_along(mesh$graph$tv[, 1]))
          ),
          data.frame(
            a = mesh$loc[mesh$graph$tv[, 2], c(1, 2)],
            b = mesh$loc[mesh$graph$tv[, 3], c(1, 2)],
            id = paste0("b", seq_along(mesh$graph$tv[, 1]))
          ),
          data.frame(
            a = mesh$loc[mesh$graph$tv[, 1], c(1, 2)],
            b = mesh$loc[mesh$graph$tv[, 3], c(1, 2)],
            id = paste0("c", seq_along(mesh$graph$tv[, 1]))
          )
        )
  colnames(df) <- c("x", "y", "x", "y", "id")

  # Cast wide to long
  # * x, y, edge id
  df <- rbind(
          df[, c(1, 2, 5)],
          df[, c(3, 4, 5)]
        )
  setDT(df)

  # Restrict to unique segments
  df %<>% .[order(x, y), .(x1 = x[1], y1 = y[1], x2 = x[2], y2 = y[2]), by = .(id)] %>%
          .[, .(x1, y1, x2, y2)] %>%
          unique()
  df[, id := 1:.N]

  # Recast wide to long
  df <- rbind(
         df[, .(id, x = x1, y = y1)],
         df[, .(id, x = x2, y = y2)]
        )

  # Convert to sf linestring
  sf <- st_as_sf(df, coords = c("x", "y"), crs = mesh$crs) %>%
        dplyr::group_by(id) %>%
        dplyr::summarise() %>%
        st_cast("LINESTRING") %>%
        st_geometry() %>%
        st_as_sf()

  # Calculate length of each segment
  sf$len <- as.numeric(st_length(sf))

  return(sf)

}

# Plot posterior marginals of fixed and random effects
plot_inla_posteriors <- function(model, group = "fixed") {

  stopifnot(is.character(group))

  if (group == "fixed") {

    posteriors <- model$marginals.fixed
    title <- "Posteriors of fixed effects"

  } else if (group == "hyperpar") {

    posteriors <- model$marginals.hyperpar
    title <- "Posteriors of hyperpars"

  } else {

    stop(glue("Unrecognized group: '{group}'"))

  }

  items <- names(posteriors) %>%
           factor(., levels = .)
  map2_df(posteriors, items, ~ data.frame(item = .y, .x)) %>%
    ggplot() +
      geom_line(aes(x = x, y = y)) +
      facet_wrap(vars(item), scales = "free") +
      ggtitle(title)

}

# Plot the PC Prior for a Matern SPDE
plot_pcmatern <- function(rho0, alpha1, sigma0, alpha2, type = "2D") {

  # The PC prior for a 2-dimensional Matern SPDE is parameterized as:
  #   P(rho < rho0) = alpha1
  #   P(sigma > sigma0) = alpha2
  # where
  #   rho = range of Matern covariance function (distance at which correlation ~= 0.14)
  #   sigma = standard deviation of Matern covariance function
  #   rho0, alpha1, sigma0, alpha2 = user-specified hyperparameters
  # The joint density of the PC prior is:
  #   P(rho, sigma) = lambda1 * rho^-2 * exp(-lambda1 * rho^-1) * lambda2 * exp(-lambda2 * sigma)
  # where
  #   lambda1 = -log(alpha1) * rho0
  #   lambda2 = -log(alpha2) / sigma0
  dpcmatern <- function(rho, sigma, rho0, alpha1, sigma0, alpha2) {
    lambda1 <- -log(alpha1) * rho0
    lambda2 <- -log(alpha2) / sigma0
    lambda1 * rho^-2 * exp(-lambda1 * rho^-1) * lambda2 * exp(-lambda2 * sigma)
  }

  # Define ranges of rho and sigma to plot over
  rho <- seq(rho0 / 100, 8 * (1 - alpha1) * rho0, length.out = 100)
  sigma <- seq(sigma0 / 100, 15 * alpha2 * sigma0, length.out = 100)

  # Title for the plot
  title <- glue("Matern PC prior: P(rho < {rho0}) = {alpha1}; P(sigma > {sigma0}) = {alpha2}")

  if (type == "2D") {

    # Marginal density of rho for fixed values of sigma
    x1 <- expand.grid(
            plot = "rho",
            rho = rho,
            sigma = round(c(0, 0.5, 1, 1.5, 2) * sigma0, 10),
            stringsAsFactors = FALSE
          )
    setDT(x1)
    x1[, density := dpcmatern(rho, sigma, rho0, alpha1, sigma0, alpha2)]
    x1[, sigma := as.factor(sigma)]

    # Marginal density of sigma for fixed values of rho
    x2 <- expand.grid(
            plot = "sigma",
            rho = round(c(0.5, 1, 5, 10) * rho0, 10),
            sigma = sigma,
            stringsAsFactors = FALSE
          )
    setDT(x2)
    x2[, density := dpcmatern(rho, sigma, rho0, alpha1, sigma0, alpha2)]
    x2[, rho := as.factor(rho)]

    # Plot both
    p <- list(
           rho = ggplot(x1) +
                   geom_line(aes(x = rho, y = density, colour = sigma)) +
                   ggtitle(title),
           sigma = ggplot(x2) +
                     geom_line(aes(x = sigma, y = density, colour = rho)) +
                     ggtitle(title)
         )

  } else if (type == "3D") {

    # Joint density of rho and sigma
    x <- expand.grid(rho = rho, sigma = sigma)
    setDT(x)
    x[, density := dpcmatern(rho, sigma, rho0, alpha1, sigma0, alpha2)]

    # Create 3D plotly plot
    p <- plotly::plot_ly(x = rho, y = sigma, z = matrix(x$density, nrow = 100, byrow = TRUE))
    p <- plotly::add_surface(p)
    p <- plotly::layout(
           p,
           title = title,
           scene = list(
                     xaxis = list(title = "rho"),
                     yaxis = list(title = "sigma"),
                     zaxis = list(title = "Density"),
                     aspectmode = "manual",
                     aspectratio = list(x = 1, y = 1, z = 0.5)
                   )
         )

  } else {

    stop(glue("Plot type must be '2D' or '3D' (got '{type}')"))

  }

  return(p)

}


# Prepare data -----

# Load db3_cc and add CV groups
db3_cc <- here("work/db/2012/db3_2012_cc.fst") %>%
          read_fst(as.data.table = TRUE)
cv_folds <- read_fst("work/db/2012/cv5_folds_2012.fst", as.data.table = TRUE) %>%
            .[db3_cc, .(cv_fold), on = .(date, sinu_id)]
db3_cc[, cv_fold := cv_folds]
rm(cv_folds)
setcolorder(db3_cc, c("date", "sinu_id", "sinu_x", "sinu_y", "cv_fold", "PM10"))



report("FIXME TODO DROPPING MISSING PM10 AND NDVI")
db3_cc %<>% .[!is.na(PM10), ]
db3_cc %<>% .[!is.na(ndvi), ]
db3_cc[, PM2.5 := NULL]



# Scale and derive new variables
# * sinu coordinates in km (rather than m)
# * sin and cosine transforms of day of week
# * temporal index (1 to number of days in year)
# * intercept column (otherwise INLA uses multiple intercepts?)
scale_data(db3_cc)
db3_cc[, `:=`(km_x = sinu_x / 1000, km_y = sinu_y / 1000)]
db3_cc[, dow_sin := sin(wday(date) / 7 * 2 * pi)]
db3_cc[, dow_cos := cos(wday(date) / 7 * 2 * pi)]
db3_cc[, t_idx := yday(date)]
db3_cc[, Intercept := 1]



# Prepare INLA -----

# Create mesh
# * MODIS sinusoidal projection with *km* units (rather than m)
# * Inner region extends ~ 30 km beyond grid points
# * Centroids of grid cells that contain PM stations become mesh nodes
#   - Sort by descending observation count so that stations with more obs will be preferred when
#     enforcing the minimum distance between mesh nodes
# * Outer buffer region extends 300 km beyond inner region
# * Max 75 km edge length in inner region; 500 km in outer
# * Min 1.5 km between nodes (reduces number of nodes by 25%)
#   - Only one node will be created for adjacent PM-containing grid cells. The node will be located
#     at one of the two centroids and the data at the other centroid will contribute to the weighted
#     average at the node
report("Building mesh")
hull <- here("work/geo/grd_1km.fst") %>%
        read_fst(columns = c("sinu_x", "sinu_y")) %>%
        magrittr::divide_by(1000) %>%
        as.matrix() %>%
        inla.nonconvex.hull(convex = 30, crs = inla.CRS(SINU_KM))
stns <- db3_cc[, .N, by = .(km_x, km_y)] %>%
        .[order(-N), ] %>%
        st_as_sf(coords = c("km_x", "km_y"), crs = SINU_KM)
mesh <- inla.mesh.2d(
          loc = as(stns, "Spatial"), # station locations
          boundary = hull,           # area of interest with ~ 30 km buffer
          offset = c(1, 300),        # extend 300 km beyond boundary to avoid edge effects
          max.edge = c(75, 500),     # max edge length (inner, outer)
          cutoff = 1.5,              # min 1.5 km between nodes
          crs = inla.CRS(SINU_KM)
        )
report(glue("Mesh nodes: {mesh$n}"))
mesh_path <- "work/explore/m3/m3_pm10_2012_inla_mesh.rds"
save_safely(mesh, saveRDS, mesh_path)
report(glue("-> {mesh_path}"), verbose)

# # Visualize mesh
# roi <- here("work/geo/roi.gpkg") %>%
#        st_read(quiet = TRUE) %>%
#        st_transform(SINU_KM)
# ggplot() +
#   geom_sf(data = roi, fill = NA) +
#   gg(mesh)[1:2] +
#   geom_sf(data = stns) +
#   coord_sf(datum = NULL) +
#   ggtitle("Mesh for 2012")
# mesh_sf <- mesh_to_sf(mesh)
# mesh_sf$len <- st_length(mesh_sf) %>% as.numeric()
# mapview(mesh_sf) + mapview(stns)

# Define SPDE for spatiotemporal random effect
# The SPDE approximates a Gaussian Markov Random Field with Matern spatial correlation:
#   s(t) ~ N(0, sigma^2 * Matern)
# It has two hyperparameters:
# * rho   = range of Matern function = distance at which correlation ~= 0.14
# * sigma = magnitude of the spatial effect
# We use penalized complexity priors that shrink the effect towards null (rho = Inf, sigma = 0)
spde <- inla.spde2.pcmatern(
          mesh = mesh,
          alpha = 2,                 # smoothness
          prior.range = c(300, 0.5), # P(range < 300 km) = 0.5 [range 95%: 56, 8300]
          prior.sigma = c(0.5, 0.5)  # P(sigma > 0.5) = 0.5    [magnitude 95%: 0.018, 2.7]
        )

# Make index to connect the spatiotemporal effect to the mesh nodes and time index
# -> list of 3 integer vectors, each of length mesh$n * number of days
#    space       = index of mesh node (loc)
#    space.group = index of time
#    space.repl  = replicate; always 1
spde_idx <- inla.spde.make.index("space", n.spde = spde$n.spde, n.group = max(db3_cc$t_idx))


# Cross-validate -----

cv_scheme <- make_cv_scheme(5)

report(glue("FIXME TODO SINGLE CV ITERATION"))
cv_iter <- cv_scheme[[8]]
dat <- db3_cc[, .(date, sinu_id, sinu_x, sinu_y, cv_fold, PM10, aod, blh_utc00, blh_utc12,
                  t2m_mean, t2m_sd, d2m_mean, sp_mean, tp_sum, u10_mean, v10_mean, tcc_mean, ndvi,
                  km_x, km_y, dow_sin, dow_cos, t_idx, Intercept)]


  # Report what's happening
  msg_prefix <- glue("[cv_inla] test {paste(cv_iter$test, collapse = ' ')}")
  msg <- map(cv_iter, paste, collapse = " ") %>%
         glue("{msg_prefix} | train {x$train}", x = .)
  report(msg)

  # Assign observations to cv sets
  # -> list of indexes identifying rows for each cv set
  cv_sets <- list(
               test = dat[cv_fold %in% cv_iter$test, which = TRUE],
               train = dat[cv_fold %in% cv_iter$train, which = TRUE]
             )

  # Create projector matrixes to link the mesh to the station locations
  # -> r x c matrix; r = nrow(dat); c = mesh$n * number of days
  trn_A <- inla.spde.make.A(
             mesh = mesh,
             loc = as.matrix(dat[cv_sets$train, .(km_x, km_y)]),
             group = dat[cv_sets$train, t_idx],
             n.group = dat[, max(t_idx)]
           )
  tst_A <- inla.spde.make.A(
             mesh = mesh,
             loc = as.matrix(dat[cv_sets$test, .(km_x, km_y)]),
             group = dat[cv_sets$test, t_idx],
             n.group = dat[, max(t_idx)]
           )

   # Create the data stack for the INLA model
  stk <- inla.stack.join(
           inla.stack(
             tag = "train",
             data = list(PM10 = dat[cv_sets$train, PM10]), # observed PM
             A = list(trn_A, 1),                           # A for GMRF; ident. matrix for covars
             effects = list(
                         spde_idx,                         # index of GMRF
                         list(dat[cv_sets$train, !"PM10"]) # covariates
                       )
           ),
           inla.stack(
             tag = "test",
             data = list(PM10 = NA), # NA -> response will be predicted
             A = list(tst_A, 1),
             effects = list(spde_idx, list(dat[cv_sets$test, !"PM10"]))
           )
         )

  # Save the data indexes of the stack; these are needed to extract fitted predictions
  stk_idx <- stk$data$index
  stk_idx_path <- paste(cv_iter$test, collapse = "_") %>%
                  glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_stack_idx.rds", x = .)
  save_safely(stk_idx, saveRDS, stk_idx_path)
  report(glue("{msg_prefix}: -> {stk_idx_path}"))


  # Experiments -----

  cv <- dat[, .(date, sinu_id, sinu_x, sinu_y, cv_fold, PM10)] %>%
        .[cv_sets$test,  cv_set := factor("test", levels = c("test", "train"))] %>%
        .[cv_sets$train, cv_set := factor("train", levels = c("test", "train"))] %>%
        setcolorder(c("date", "sinu_id", "sinu_x", "sinu_y", "cv_fold", "cv_set"))

  # m3_ar1: log(PM) ~ covars + ar1(s, t) -----

  # Fit
  report(glue("{msg_prefix}: fit log(PM) ~ covars + ar1(s, t)"))
  m3_ar1 <- inla(
    log(PM10) ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + t2m_sd + d2m_mean +
                sp_mean + tp_sum + u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
                f(space, model = spde, group = space.group, control.group = list(
                  model = "ar1", hyper = list(rho  = list(prior = "pc.cor0", param = c(0.9, 0.01))))),
    data = inla.stack.data(stk),
    family = "gaussian",
    control.family = list(hyper = list(prec = list(prior = "pc.prec", param = c(1, 0.1)))),
    control.predictor = list(A = inla.stack.A(stk), compute = TRUE),
    control.mode = list(restart = TRUE, theta = theta),
    control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
    control.inla = list(int.strategy = "eb")
  )
  print(summary(m3_ar1))

  # Save
  m3_path <- paste(cv_iter$test, collapse = "_") %>%
             glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_ar1.rds", x = .)
  report(glue("{msg_prefix}: saving model"))
  save_safely(m3_ar1, saveRDS, m3_path)
  report(glue("{msg_prefix}: -> {m3_path}"))

  # Report performance
  for (set in c("train", "test")) {
    idx <- stk_idx[[set]]
    p <- exp(m3_ar1$summary.fitted.values$mean[idx])
    cv[cv_sets[[set]], m3_ar1 := p]
  }
  rm(idx, set, p)
  report(glue("{msg_prefix}: CV performance"))
  cv[, assess_preds(.SD, "PM10", "m3_ar1", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()


  # m3_nolog: PM ~ covars + ar1(s, t) -----

  # Fit
  report(glue("{msg_prefix}: fit PM10 ~ covars + ar1(s, t)"))
  m3_nolog <- inla(
    PM10 ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + d2m_mean + sp_mean + tp_sum +
           u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
           f(space, model = spde, group = space.group, control.group = list(
             model = "ar1", hyper = list(rho = list(prior = "pc.cor1", param = c(0, 0.95))))),
    data = inla.stack.data(stk), family = "gaussian",
    control.family = list(hyper = list(prec = list(prior = "pc.prec", param = c(1, 0.1)))),
    control.predictor = list(A = inla.stack.A(stk), compute = TRUE),
    control.mode = list(restart = TRUE, theta = log(c(0.076, 479, 206, 4410))),
    control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
    control.inla = list(int.strategy = "eb")
  )
  print(summary(m3_nolog))

  # Save
  paste(cv_iter$test, collapse = "_") %>%
    glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_nolog.rds", x = .) %>%
    save_safely(m3_nolog, saveRDS, .)

  # Report performance
  for (set in c("train", "test")) {
    idx <- stk_idx[[set]]
    p <- m3_nolog$summary.fitted.values$mean[idx]
    cv[cv_sets[[set]], m3_nolog := p]
  }
  rm(idx, set, p)
  cv[, assess_preds(.SD, "PM10", "m3_nolog", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()


  # m3_iid: log(PM) ~ covars + iid(s, t) -----

  # Fit
  report(glue("{msg_prefix}: fit PM10 ~ covars + ar1(s, t)"))
  m3_iid <- inla(
    log(PM10) ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + d2m_mean + sp_mean +
                tp_sum + u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
                f(space, model = spde, group = space.group, control.group = list(
                  model = "iid", hyper = list(prec = list(prior = "pc.prec", param = c(1, 0.1))))),
    data = inla.stack.data(stk), family = "gaussian",
    control.family = list(hyper = list(prec = list(prior = "pc.prec", param = c(1, 0.1)))),
    control.predictor = list(A = inla.stack.A(stk), compute = TRUE),
    control.mode = list(restart = TRUE, theta = log(c())),
    control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
    control.inla = list(int.strategy = "eb")
  )
  print(summary(m3_iid))

  # Save
  paste(cv_iter$test, collapse = "_") %>%
    glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_iid.rds", x = .) %>%
    save_safely(m3_iid, saveRDS, .)

  # Report performance
  for (set in c("train", "test")) {
    idx <- stk_idx[[set]]
    p <- exp(m3_iid$summary.fitted.values$mean[idx])
    cv[cv_sets[[set]], m3_iid := p]
  }
  rm(idx, set, p)
  cv[, assess_preds(.SD, "PM10", "m3_iid", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()


  # m3_iid_iid: log(PM) ~ covars + iid(s, t) + aod*iid(s, t) -----

  # Create second SPDE for random slope
  spde2 <- inla.spde2.pcmatern(mesh, alpha = 2, prior.range = c(300, 0.5), prior.sigma = c(0.25, 0.5))
  spde_idx2 <- inla.spde.make.index("s_aod_slp", n.spde = spde2$n.spde, n.group = max(db3_cc$t_idx))
  stk2 <- inla.stack.join(
           inla.stack(
             tag = "train",
             data = list(PM10 = dat[cv_sets$train, PM10]),
             A = list(trn_A, trn_A, 1),
             effects = list(spde_idx, spde_idx2, list(dat[cv_sets$train, !"PM10"]))
           ),
           inla.stack(
             tag = "test",
             data = list(PM10 = NA),
             A = list(tst_A, tst_A, 1),
             effects = list(spde_idx, spde_idx2, list(dat[cv_sets$test, !"PM10"]))
           )
         )

  # Fit
  m3_iid_iid <- inla(
    log(PM10) ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + t2m_sd + d2m_mean + sp_mean +
                tp_sum + u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
                f(space, model = spde, group = space.group, control.group = list(
                    model = "iid", hyper = list(prec = list(prior = "pc.prec", param = c(0.5, 0.5))))) +
                f(s_aod_slp, aod, model = spde2, group = s_aod_slp.group, control.group = list(
                    model = "iid", hyper = list(prec = list(prior = "pc.prec", param = c(0.5, 0.5))))),
     family = "gaussian", data = inla.stack.data(stk2),
     control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
     control.predictor = list(A = inla.stack.A(stk2), compute = TRUE),
     control.family = list(hyper = list(prec = list(prior = "pc.prec", param = c(0.5, 0.5)))),
     control.inla = list(int.strategy = "eb"),
     control.mode = list(restart = TRUE, theta = log(c(15, 530, 0.4, 530, 0.4)))
  )
  print(summary(m3_iid_iid))

  # Save
  paste(cv_iter$test, collapse = "_") %>%
    glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_iid_iid.rds", x = .) %>%
    save_safely(m3_iid_iid, saveRDS, .)

  # Report performance
  for (set in c("train", "test")) {
    idx <- inla.stack.index(stk2, set)$data
    p <- exp(m3_iid_iid$summary.fitted.values$mean[idx])
    cv[cv_sets[[set]], m3_iid_iid := p]
  }
  rm(idx, set, p)
  cv[, assess_preds(.SD, "PM10", "m3_iid_iid", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()


  # m3_ar1_iid: log(PM) ~ covars + ar1(s, t) + aod*iid(s, t) -----

  # Fit
  m3_ar1_iid <- inla(
    log(PM10) ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + t2m_sd + d2m_mean + sp_mean +
                tp_sum + u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
                f(space, model = spde, group = space.group, control.group = list(
                  model = "ar1", hyper = list(rho = list(prior = "pc.cor0", param = c(0.9, 0.01))))) +
                f(s_aod_slp, aod, model = spde2, group = s_aod_slp.group, control.group = list(
                  model = "iid", hyper = list(prec = list(prior = "pc.prec", param = c(0.5, 0.5))))),
     family = "gaussian", data = inla.stack.data(stk2),
     control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
     control.predictor = list(A = inla.stack.A(stk2), compute = TRUE),
     control.family = list(hyper = list(prec = list(prior = "pc.prior", param = c(1, 0.1)))),
     control.inla = list(int.strategy = "eb"),
     control.mode = list(restart = TRUE, theta = log(c(46.7, 27.6, 1.79, 6453, 556, 0.32)))
  )
  print(summary(m3_ar1_iid))

  # Save
  paste(cv_iter$test, collapse = "_") %>%
    glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_ar1_iid.rds", x = .) %>%
    save_safely(m3_ar1_iid, saveRDS, .)

  # Report performance
  for (set in c("train", "test")) {
    idx <- inla.stack.index(stk2, set)$data
    p <- exp(m3_ar1_iid$summary.fitted.values$mean[idx])
    cv[cv_sets[[set]], m3_ar1_iid := p]
  }
  rm(idx, set, p)
  cv[, assess_preds(.SD, "PM10", "m3_ar1_iid", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()


  # Save expermient CV predictions -----
  paste(cv_iter$test, collapse = "_") %>%
    glue("work/explore/m3/m3_pm10_2012_inla_test_{x}_compare_cv.fst", x = .) %>%
    save_safely(pred, write_fst, ., compress = 100)


  # Predict on full grid for Jan 2012 -----

  # Naively predict daily PM10 for France in Jan 2012
  # "Naively" because we simply take the posterior means of the fixed and random effects, multiply
  # them by the covariates for each grid cell-day, and sum. This gives the posterior mean PM10 for
  # each cell day but *not* the full posterior marginal distribution. The simplest way to get the
  # posterior marginal distribution would be to refit the model with a stack that includes every
  # cell-day, but this runs out of memory (on dahu with 187 GB RAM). The naive approach could also
  # be used to get the standard deviation of the posterior, but we would need to account for the
  # variance in each effect *and* the covariance between the effects.

  # Load db3 for Jan 2012
  db3 <- read_fst("work/db/2012/db3_2012-01.fst", as.data.table = TRUE) %>%
         scale_data() %>%
         .[, dow_sin := sin(wday(date) / 7 * 2 * pi)] %>%
         .[, dow_cos := cos(wday(date) / 7 * 2 * pi)] %>%
         .[, t_idx := yday(date)] %>%
         .[, Intercept := 1]

  db3[, m3_ar1   := inla_predict_on_grid(db3, m3_ar1,   mesh, spde_idx$space.group, exp)]
  db3[, m3_nolog := inla_predict_on_grid(db3, m3_nolog, mesh, spde_idx$space.group)]
  db3[, m3_iid   := inla_predict_on_grid(db3, m3_iid,   mesh, spde_idx$space.group, exp)]

  # Predictions for models with a random slope is a little more complicated
  fixef <- inla_fixef_on_grid(db3, m3_iid_iid)
  ranef1 <- inla_ranef_on_grid(db3, mesh, m3_iid_iid$summary.random$space$mean,
                               spde_idx$space.group)
  ranef2 <- inla_ranef_on_grid(db3, mesh, m3_iid_iid$summary.random$s_aod_slp$mean,
                               spde_idx2$s_aod_slp.group)
  db3[, m3_iid_iid := exp(fixef + ranef1 + (ranef2 * aod))]
  rm(fixef, ranef1, ranef2)

  fixef <- inla_fixef_on_grid(db3, m3_ar1_iid)
  ranef1 <- inla_ranef_on_grid(db3, mesh, m3_ar1_iid$summary.random$space$mean,
                               spde_idx$space.group)
  ranef2 <- inla_ranef_on_grid(db3, mesh, m3_ar1_iid$summary.random$s_aod_slp$mean,
                               spde_idx2$s_aod_slp.group)
  db3[, m3_ar1_iid := exp(fixef + ranef1 + (ranef2 * aod))]
  rm(fixef, ranef1, ranef2)

  # Save
  db3[, .(date, sinu_id, sinu_x, sinu_y, t_idx, PM10, aod, aod_maiac, m3_ar1, m3_nolog, m3_iid,
          m3_iid_iid, m3_ar1_iid)] %>%
    save_safely(write_fst, "work/explore/m3/m3_pm10_2012_inla_compare_pred.fst", compress = 100)


  # Further exploration -----

  # Further exploration (e.g. limited timeperiod, northwest only) confirms that ar1 model overfits:
  # When holding out stations (rather than obs) for CV, for Jan - Jun in northwest:
  # ar1 RMSE 7.35 / R2 0.78
  # iid RMSE 6.67 / R2 0.83

  # Limit to northwest in Jan - Jun
  dat <- db3_cc[month(date) < 7, ] %>%
         .[between(sinu_x, -35e5, 1e5) & between(sinu_y, 51.1e5, 56e5), ] %>%
         .[, .(date, sinu_id, sinu_x, sinu_y, cv_fold, PM10, aod, blh_utc00, blh_utc12,
               t2m_mean, t2m_sd, d2m_mean, sp_mean, tp_sum, u10_mean, v10_mean, tcc_mean, ndvi,
               km_x, km_y, dow_sin, dow_cos, t_idx, Intercept)]
  dat[, t_idx := as.integer(as.factor(t_idx))]

  # CV by station
  set.seed(1)
  cv_folds <- dat[, .N, by = .(sinu_id)] %>%
              .[, cv_fold := sample(rep(1:5, length = .N))] %>%
              .[dat, cv_fold, on = .(sinu_id)]
  dat[, cv_fold := cv_folds]
  dat[, .N, keyby = .(cv_fold)]

  cv_scheme <- make_cv_scheme(5)
  cv_iter <- cv_scheme[[8]]
  cv_sets <- list(
               test = dat[cv_fold %in% cv_iter$test, which = TRUE],
               train = dat[cv_fold %in% cv_iter$train, which = TRUE]
             )

  # Prepare for INLA
  mesh <- readRDS("work/explore/m3/m3_pm10_2012_inla_mesh.rds")
  spde <- inla.spde2.pcmatern(mesh, prior.range = c(300, 0.5), prior.sigma = c(0.5, 0.5))
  spde_idx <- inla.spde.make.index("space", n.spde = spde$n.spde, n.group = dat[, uniqueN(t_idx)])
  trn_A <- inla.spde.make.A(
             mesh = mesh,
             loc = as.matrix(dat[cv_sets$train, .(km_x, km_y)]),
             group = dat[cv_sets$train, t_idx],
             n.group = dat[, max(t_idx)]
           )
  tst_A <- inla.spde.make.A(
             mesh = mesh,
             loc = as.matrix(dat[cv_sets$test, .(km_x, km_y)]),
             group = dat[cv_sets$test, t_idx],
             n.group = dat[, max(t_idx)]
           )
  stk <- inla.stack.join(
           inla.stack(
             tag = "train",
             data = list(PM10 = dat[cv_sets$train, PM10]), # observed PM
             A = list(trn_A, 1),                           # A for GMRF; ident. matrix for covars
             effects = list(
                         spde_idx,                         # index of GMRF
                         list(dat[cv_sets$train, !"PM10"]) # covariates
                       )
           ),
           inla.stack(
             tag = "test",
             data = list(PM10 = NA), # NA -> response will be predicted
             A = list(tst_A, 1),
             effects = list(spde_idx, list(dat[cv_sets$test, !"PM10"]))
           )
         )

  cv <- dat[, .(date, sinu_id, sinu_x, sinu_y, cv_fold, PM10)] %>%
        .[cv_sets$test,  cv_set := factor("test", levels = c("test", "train"))] %>%
        .[cv_sets$train, cv_set := factor("train", levels = c("test", "train"))] %>%
        setcolorder(c("date", "sinu_id", "sinu_x", "sinu_y", "cv_fold", "cv_set"))

  # ar1 model
  nw_ar1 <- inla(
    log(PM10) ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + t2m_sd + d2m_mean +
                sp_mean + tp_sum + u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
                f(space, model = spde, group = space.group, control.group = list(
                  model = "ar1", hyper = list(rho = list(prior = "pc.cor0", param = c(0.7, 0.01))))),
    data = inla.stack.data(stk),
    family = "gaussian",
    control.family = list(hyper = list(prec = list(prior = "pc.prec", param = c(1, 0.1)))),
    control.predictor = list(A = inla.stack.A(stk), compute = TRUE),
    control.mode = list(restart = TRUE, theta = c(4.0162, 6.4729, 0.7185, 5.0873)),
    control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
    # verbose = TRUE,
    control.inla = list(int.strategy = "eb")
  )
  print(summary(nw_ar1))
  nw_ar1$mode$theta %>% round(4) %>% paste(collapse = ", ")

  for (set in c("train", "test")) {
    idx <- inla.stack.index(stk, set)$data
    p <- exp(nw_ar1$summary.fitted.values$mean[idx])
    cv[cv_sets[[set]], pred_nw_ar1 := p]
  }
  rm(idx, set, p)
  report("nw_ar1:")
  cv[, assess_preds(.SD, "PM10", "pred_nw_ar1", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()
  # 2020-05-18 11:34:44: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
  # 2020-05-18 11:34:44:   test 7.3452 0.7829 5.1207     4.2452   0.0308    5.0213  0.8812 -25.2326 47.2565  3861  25.7713 15.2073
  # 2020-05-18 11:34:44:  train 2.7179 0.9687 1.8112     0.4297   0.9858    2.6894  0.9673 -12.1328 56.8115  6102  24.8499 15.3253

  # iid model
  nw_iid <- inla(
    log(PM10) ~ 0 + Intercept + aod + blh_utc00 + blh_utc12 + t2m_mean + t2m_sd + d2m_mean +
                sp_mean + tp_sum + u10_mean + v10_mean + tcc_mean + ndvi + dow_sin + dow_cos +
                f(space, model = spde, group = space.group, control.group = list(
                  model = "iid", hyper = list(prec = list(prior = "pc.prec", param = c(0.7, 0.5))))),
    data = inla.stack.data(stk),
    family = "gaussian",
    control.family = list(hyper = list(prec = list(prior = "pc.prec", param = c(0.5, 0.5)))),
    control.predictor = list(A = inla.stack.A(stk), compute = TRUE),
    control.mode = list(restart = TRUE, theta = c(3.5960, 6.0455, -1.0051)),
    control.compute = list(openmp.strategy = "pardiso.parallel", dic = TRUE),
    # verbose = TRUE,
    control.inla = list(int.strategy = "eb")
  )
  print(summary(nw_iid))
  nw_iid$mode$theta %>% round(4) %>% paste(collapse = ", ")

  for (set in c("train", "test")) {
    idx <- inla.stack.index(stk, set)$data
    p <- exp(nw_iid$summary.fitted.values$mean[idx])
    cv[cv_sets[[set]], pred_nw_iid := p]
  }
  rm(idx, set, p)
  report("nw_iid:")
  cv[, assess_preds(.SD, "PM10", "pred_nw_iid", "sinu_id"), keyby = .(cv_set)] %>%
    .[, lapply(.SD, round, 4), keyby = .(cv_set)] %>%
    .[, .(cv_set, rmse, r2, mae, rmse.space, r2.space, rmse.time, r2.time, res.min, res.max,
          obs.n, obs.mean, obs.sd)] %>%
    report()
  # 2020-05-18 11:34:49: cv_set   rmse     r2    mae rmse.space r2.space rmse.time r2.time  res.min res.max obs.n obs.mean  obs.sd
  # 2020-05-18 11:34:49:   test 6.6723 0.8252 4.5155     4.1465   0.0753    4.5018  0.9045 -22.2132 48.2144  3861  25.7713 15.2073
  # 2020-05-18 11:34:49:  train 3.0299 0.9616 2.1037     1.1901   0.8907    2.7689  0.9654 -13.5371 54.4242  6102  24.8499 15.3253


  # Look at performance of distant stations
  stns <- cv[, .(.N, pm_mean = mean(PM10)), by = .(sinu_id, sinu_x, sinu_y, cv_set)] %>%
          grid_to_sf()
  stns$dist <- st_distance(stns, stns[stns$cv_set == "train", ]) %>%
               apply(1, min)
  cv[, dist := as.data.table(stns)[cv, .(dist), on = .(sinu_id)]]
  cv[cv_set == "test", .(mae = mae(PM10 - pred_nw_iid), rmse = rmse(PM10 - pred_nw_iid)),
      by = .(sinu_id, sinu_x, sinu_y, dist)] %>%
    ggplot() +
      geom_point(aes(x = dist, y = mae))

  cv[cv_set == "test", .(pm_mean = mean(PM10), mae = mae(PM10 - pred_nw_iid), rmse = rmse(PM10 - pred_nw_iid)),
      by = .(sinu_id, sinu_x, sinu_y, dist)] %>%
    .[order(mae), ] %>%
    grid_to_sf() %>%
    mapview(zcol = "mae")

  # Predict in northwest
  db3 <- read_fst("work/db/2012/db3_2012-02.fst", as.data.table = TRUE) %>%
         .[between(sinu_x, -35e5, 1e5) & between(sinu_y, 51.1e5, 56e5), ] %>%
         scale_data() %>%
         .[, dow_sin := sin(wday(date) / 7 * 2 * pi)] %>%
         .[, dow_cos := cos(wday(date) / 7 * 2 * pi)] %>%
         .[, t_idx := as.integer(as.factor(date))] %>%
         .[, Intercept := 1]
  db3[, pred_nw_ar1 := inla_predict_on_grid(db3, nw_ar1, mesh, spde_idx$space.group, trafo = exp)]
  db3[, pred_nw_iid := inla_predict_on_grid(db3, nw_iid, mesh, spde_idx$space.group, trafo = exp)]

  # Map predictions
  pred_r <- db3[, .(sinu_x, sinu_y, pred_nw_ar1, pred_nw_iid)] %>%
            split(db3$t_idx) %>%
            .[c(7, 14, 21, 28)] %>%
            map(sinu_to_raster) %>%
            stack
  plot(pred_r, nc = 4)
  mapview(pred_r, alpha = 1, na.color = NA)

  # Plot predictions vs. obs
  cv[cv_set == "test" & month(date) == 2, ] %>%
    ggplot() +
      geom_line(aes(x = date, y = PM10)) +
      geom_line(aes(x = date, y = pred_nw_ar1), colour = "blue") +
      geom_line(aes(x = date, y = pred_nw_iid), colour = "red") +
      facet_wrap(vars(sinu_id))

  # Map random field
  pj <- inla.mesh.projector(
          mesh,
          xlim = c(-350, 150),
          ylim = c(5100, 5600),
          dim = c(500, 500),
          crs = mesh$crs
        )
  gf_ar1_r <- nw_ar1$summary.random$space$mean %>%
              split(spde_idx$space.group) %>%
              map(inla_field_to_raster, projector = pj) %>%
              stack()
  gf_iid_r <- nw_iid$summary.random$space$mean %>%
              split(spde_idx$space.group) %>%
              map(inla_field_to_raster, projector = pj) %>%
              stack()
  names(gf_ar1_r) <- paste0("ar1_", 1:nlayers(gf_ar1_r))
  names(gf_iid_r) <- paste0("iid_", 1:nlayers(gf_iid_r))
  gf_r <- stack(gf_ar1_r, gf_iid_r) %>%
          exp()
  rm(gf_ar1_r, gf_iid_r)

  plot(gf_r[[c("ar1_7", "ar1_14", "ar1_21", "iid_7", "iid_14", "iid_21")]], nc = 3)

  mapview(gf_r$ar1_7, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$iid_7, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$ar1_14, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$iid_14, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$ar1_21, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$iid_21, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$ar1_28, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(gf_r$iid_28, alpha = 1, na.color = NA, use.layer.names = T) +
    mapview(stns, zcol = "cv_set")
