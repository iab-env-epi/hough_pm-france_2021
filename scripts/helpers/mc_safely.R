# Wrapper around mclapply that raises an error if any of the forked processes are killed
# For running on CIMENT, which kills individual processes if they cause a job to exceed its total
# allocated memory. If a process forked by mclapply is killed in this way it returns NULL and
# mclapply issues a warning but **DOES NOT ERROR** -> the code continues with incomplete results.
mc_safely <- function(X, FUN, ..., mc.preschedule = TRUE, mc.cores = 1L) {

  stop_if_any_failed <- function(w) {
    regx <- "\\d+ parallel jobs? did not deliver"
    if (w$call == "mccollect(jobs)" && grepl(regx, w$message)) stop(w$message)
  }

  withCallingHandlers(
    mclapply(X, FUN, ..., mc.preschedule = mc.preschedule, mc.cores = mc.cores),
    warning = stop_if_any_failed
  )

}
