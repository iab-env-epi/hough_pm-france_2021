# Load a GeoTIFF as a RasterBrick and assign layer names based on an accompanying yml file
load_geotiff <- function(x, ...) {

  brk <- raster::brick(x, ...)

  # Load names from a yml file if it exists
  yml_path <- paste0(x, ".yml")
  if (file.exists(yml_path)) {
    names(brk) <- yaml::read_yaml(yml_path)
  }

  brk

}

# Save a raster as a GeoTIFF with an accompanying yml file that lists the layer names
save_geotiff <- function(x, filename, ...) {

  yaml::write_yaml(names(x), paste0(filename, ".yml"))
  raster::writeRaster(x, filename, ...)

}
