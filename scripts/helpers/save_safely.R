# Save an object to a temp file and then move to final destination
save_safely <- function(x, fun, path, ...) {

  # Ensure the destination dir exists
  dir.create(dirname(path), recursive = TRUE, showWarnings = FALSE)

  # Save to a temp file and then rename
  # * Must prefix tmp to extension b/c some functions use extension to determine output format
  # * ggsave requires non-standard arguments in non-standard order
  tmp <- sub("\\.(\\w+)$", ".tmp.\\1", path)
  if (deparse(substitute(fun)) == "ggsave") {

    result = fun(filename = basename(tmp), plot = x, path = dirname(tmp), ...)

  } else {

    result <- fun(x, tmp, ...)

  }
  file.rename(tmp, path)

  # Rename extra files e.g. shapefile or GeoTIFFs with layer names yml
  rename_extras <- function(base, ptrn) {

    files <- list.files(dirname(tmp), pattern = paste0(base, ptrn), full.names = T)
    for (f in files) {
      file.rename(f, sub(ptrn, ".\\1", f))
    }

  }
  if (grepl("\\.shp$", path)) {

    rename_extras(
      base = sub("\\.shp$", "", basename(path)), # file name without any extension
      ptrn = ".tmp.(dbf|prj|shx)$"               # file extensions to be renamed
    )

  } else if (grepl("\\.tif$", path)) {

    rename_extras(
      base = sub("\\.tif$", "", basename(path)), # file name without any extension
      ptrn = ".tmp.(tif|tif.yml|tif.aux.xml)$"   # file extensions to be renamed
    )

  }

  # Return the output of the save function
  invisible(result)

}
