# Combine EPSG:3035 LAEA Europe x and y coordinates using a Cantor pair
# Rounds x and y to the nearest meter b/c this produces hashes up to 1303563329892194 (~ 1.3e15);
# R only has precision of 1 for values >= 1e14
hash_laea <- function(x, y) {

  x <- round(x * 10) # LAEA x range [0, 7320000]
  y <- round(y * 10) # LAEA y range [0, 5450000]

  # Cantor pair
  # https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
  0.5 * (x + y) * (x + y + 1) + x

}

# Combine MODIS sinusoidal x and y coordinates using a Cantor pair
# Rounds x and y to the nearest meter (max precision for R float given coordinate range)
hash_sinu <- function(x, y) {

  x <- round(x + 20015110)
  y <- round(y + 10007555)

  # Cantor pair
  # https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
  0.5 * (x + y) * (x + y + 1) + x

}

# Unhash a Cantor pair of MODIS sinusoidal x and y coordinates
# Returns coordinates rounded to nearest meter
unhash_laea <- function(hash) {

  w <- floor((sqrt(8 * hash + 1) - 1) / 2)
  t <- w * (w + 1) / 2
  x <- (hash - t) / 10
  y <- (w - hash + t) / 10

  list(x = x, y = y)

}

# Unhash a Cantor pair of MODIS sinusoidal x and y coordinates
# Returns coordinates rounded to nearest meter
unhash_sinu <- function(hash) {

  w <- floor((sqrt(8 * hash + 1) - 1) / 2)
  t <- w * (w + 1) / 2
  x <- hash - t - 20015110
  y <- w - hash + t - 10007555

  list(x = x, y = y)

}

# # https://mmcloughlin.com/posts/geohash-assembly
#
# // Quantize maps latitude and longitude to 32-bit integers.
# func Quantize(lat, lng float64) (lat32 uint32, lng32 uint32) {
#   lat32 = uint32(math.Ldexp((lat+90.0)/180.0, 32))
#   lng32 = uint32(math.Ldexp((lng+180.0)/360.0, 32))
#   return
# }
#
# // Spread out the 32 bits of x into 64 bits, where the bits of x occupy even
# // bit positions.
# func Spread(x uint32) uint64 {
#   X := uint64(x)
#   X = (X | (X << 16)) & 0x0000ffff0000ffff
#   X = (X | (X << 8)) & 0x00ff00ff00ff00ff
#   X = (X | (X << 4)) & 0x0f0f0f0f0f0f0f0f
#   X = (X | (X << 2)) & 0x3333333333333333
#   X = (X | (X << 1)) & 0x5555555555555555
#   return X
# }
#
# // Interleave the bits of x and y. In the result, x and y occupy even and odd
# // bitlevels, respectively.
# func Interleave(x, y uint32) uint64 {
#   return Spread(x) | (Spread(y) << 1)
# }
