#
# Functions for m2: MAIAC AOD ~ EAC4 AOD
# Impute (gapfill) missing MODIS MAIAC AOD based on ECMWF atmospheric composition reanalysis 4 AOD
#

# Assemble the components needed for m2
# * Load data and assign spatiotemporal blocks for cross-validation
# * Define the (monthly) gapfilling task and features
# * Configure the random forest: 96 trees, no replacement
# * Define the resampling for tuning and CV: 5-fold blocked (daily clusters)
prep_m2 <- function(data_env, db2_cc_path, day, ncores) {

  # Load the AOD gapfilling db (MAIAC AOD and EAC4/MERRA-2 AOD)
  # Read into the environment that is shared between iterations of the loop
  if (!exists("db2_cc", where = data_env, inherits = FALSE)) {

    data_env$db2_cc <- read_fst(db2_cc_path, as.data.table = TRUE)

  }

  # Assign each cell-day to a spatiotemporal block for cross-validation
  # * 50 spatially contiguous blocks per day (not all blocks have data on every day)
  # * Test cells are far from same-day train cells
  # * Test cells may be close (or identical) to other-day train cells
  if (!("cv_block" %in% names(data_env$db2_cc))) {

    blocks <- str_replace(db2_cc_path, "_cc.fst$", "_blocks.fst") %>%
              read_fst(as.data.table = TRUE)
    dt_left_join(data_env$db2_cc, blocks)
    rm(blocks)

  }
  names(data_env$db2_cc) %>% paste(collapse = " ")
  # date sinu_id sinu_x sinu_y aod_047 dow doy time aod_055 maiac_n aod469_utc00
  # aod469_utc03 aod469_utc06 aod469_utc09 aod469_utc12 aod469_utc15 aod469_utc18
  # aod469_utc21 cv_block

  # Get the index of the data for the current month
  idx <- data_env$db2_cc[month(date) == month(day), which = TRUE]

  # Define the features and gapfilling task
  # Different features for 2000-2002 b/c uses MERRA2 AOD rather than EAC4 AOD
  if (year(day) %in% EAC4_YEARS) {
    feats <- c("sinu_x", "sinu_y", "dow", "doy", "aod469_utc00", "aod469_utc03", "aod469_utc06",
               "aod469_utc09", "aod469_utc12", "aod469_utc15", "aod469_utc18", "aod469_utc21")
  } else {
    feats <- c("sinu_x", "sinu_y", "dow", "doy", "aod550_utc08", "aod550_utc09", "aod550_utc10",
               "aod550_utc11", "aod550_utc12", "aod550_utc13", "aod550_utc14", "aod550_utc15")
  }
  task <- mlr::makeRegrTask(
            id = glue("m2_{format(day, '%Y-%m')}"),
            data = data_env$db2_cc[idx, c("aod_047", ..feats)] %>%
                   setDF(),
            target = "aod_047",
            blocking = data_env$db2_cc[idx, cv_block]
          )

  # Define the learner and resampling method
  # * Random Forest
  #   - 96 trees (faster than 100 with 32 cores, more trees barely improves)
  #   - sample without replacement (faster, better for estimating feature importance)
  # * 5-fold CV with daily spatial blocks
  lrnr <- mlr::makeLearner(
            cl = "regr.ranger",
            num.trees = 96,
            replace = FALSE,
            num.threads = ncores
          )
  rsmp <- mlr::makeResampleDesc(method = "CV", iters = 5, blocking.cv = TRUE)

  # Return
  list(data_idx = idx, task = task, resampling = rsmp, learner = lrnr)

}

# Tune mtry on a single 50% sample of the data
# * Faster than nested tuning within CV (~ 50 min / month vs. > 3 hours / month)
# * 50% sample gives good estimate of optimal mtry (25% sample did not)
# * May bias the CV results b/c 50% of test data used for tuning -> we are in part choosing the mtry
#   that works best for the test data. However, optimal mtry seems stable across different 50%
#   samples so nested tuning within CV would probably give same mtry (unlikely to be biased)
# ~ 10h00 (8h15 - 12h00 per year; 30 min - 2h00 / month: slower if more data and higher mtry)
tune_m2 <- function(year, months = study_months(year), ncores = 1L, force = FALSE, verbose = FALSE) {

  # Ensure all months are in study period (exclude Jan + Feb 2000 b/c MAIAC AOD begins 2000-02-26)
  if (!missing(months)) {

    check <- setdiff(months, study_months(year))
    if (length(check) > 0) {
      stop(glue("Months [{paste(check, collapse = ' ')}] not in study period for {year}"))
    }

  }

  # Skip if already processed
  tune_paths <- lubridate::make_date(year, months, 1) %>%
                purrr::set_names() %>%
                purrr::map_chr(function(day) {
                  here(glue("work/models/{year}/m2_{format(day, '%Y-%m')}_tune.rds"))
                })
  if (all_exist(tune_paths, force = force)) return(as.character(tune_paths))

  report(glue("[tune_m2] {year}: tune m2 [MAIAC AOD ~ RF(EAC4/MERRA2 AOD)]"))

  # Create a new environment to hold the data
  # This avoids loading data unless it is needed by an iteration of the loop over months
  data_env = new.env()

  # Tune by month
  # Training time increases ~ exponentially with number of rows
  path_missing(tune_paths, force = FORCE) %>%
    purrr::iwalk(function(tune_path, day, data_env) {

      day %<>% lubridate::ymd()
      ym <- format(day, "%Y-%m")

      msg_prefix <- glue("[tune_m2] {ym}:")

      # Load db2_cc and define the task, learner, and resampling
      report(glue("{msg_prefix} assembling m2 components"), verbose)
      parts <- here(glue("work/db/{year}/db2_{year}_cc.fst")) %>%
               prep_m2(data_env = data_env, db2_cc_path = ., day = day, ncores = ncores)

      # Define a measure that reports the test set size
      test_n <- mlr::makeMeasure(
                  id = "test_n",
                  minimize = FALSE,
                  properties = c("regr", "req.pred", "req.task"),
                  fun = function(task, model, pred, feats, extra.args) {
                          nrow(as.data.frame(pred))
                        }
                )

      # Tune mtry on a random 50% sample of the data
      # * Minimize MAE using same resampling as for CV
      # * Use set.seed for reproducibility
      # * mlrMBO with design grid of common optimal values
      # * suppress mlrMBO warning when < 1000 possible hyperparameter combinations
      report(glue("{msg_prefix} tuning"), verbose)
      set.seed(100)
      tune <- withCallingHandlers(
                mlr::tuneParams(
                  learner = parts$learner,
                  task = mlr::downsample(parts$task, perc = 0.5),
                  resampling = parts$resampling,
                  measures = list(mlr::mae, mlr::rsq, mlr::rmse, test_n),
                  par.set = makeParamSet(
                              makeIntegerParam("mtry", 1, mlr::getTaskNFeats(parts$task))
                            ),
                  control = mlr::makeTuneControlMBO(
                              budget = 7,
                              mbo.design = data.frame(mtry = 1:4)
                            ),
                  show.info = verbose
                ),
                warning = muffle_mlrMBO_tune_warning
              )
      set.seed(Sys.time())
      # 2012-01 mtry=1 r2=0.545 mae=0.0258
      # 2012-02 mtry=1 r2=0.709 mae=0.0303
      # 2012-03 mtry=4 r2=0.821 mae=0.0287
      # 2012-04 mtry=4 r2=0.807 mae=0.0383 [should have tested 5 also]
      # 2012-05 mtry=2 r2=0.833 mae=0.0340 [mtry=1 may have slightly better R2 + RMSE]
      # 2012-06 mtry=3 r2=0.801 mae=0.0362 [mtry=1 may have slightly better R2 + RMSE]
      # 2012-07 mtry=2 r2=0.805 mae=0.0271 [mtry=1 may have slightly better R2 + RMSE]
      # 2012-08 mtry=2 r2=0.854 mae=0.0277
      # 2012-09 mtry=3 r2=0.863 mae=0.0277
      # 2012-10 mtry=1 r2=0.678 mae=0.0257
      # 2012-11 mtry=1 r2=0.508 mae=0.0249
      # 2012-12 mtry=1 r2=0.459 mae=0.0250

      # Report the tune result
      report(glue("{msg_prefix} {names(tune$x)} = {tune$x} ({names(tune$y)} = {round(tune$y, 5)})"))

      # Save the tune result (remove mbo result to reduce size)
      tune$mbo.result <- NULL
      save_safely(tune, fun = saveRDS, path = tune_path)
      report(glue("{msg_prefix} -> {tune_path}"))

      # Plot the tune result
      p <- mlr::generateHyperParsEffectData(tune)$data %>%
           as.data.table() %>%
           .[, .(mtry, mae = mae.test.mean, r.squared = rsq.test.mean)] %>%
           melt(id.vars = "mtry", variable.name = "measure") %>%
           ggplot(aes(x = mtry, y = value)) +
             geom_point() +
             facet_grid(measure ~ ., scales = "free_y") +
             ggtitle(glue("m2 {ym} tune"))
      p_path <- here(glue("work/review/{year}/m2_{ym}_tune.png"))
      save_safely(p, fun = ggsave, path = p_path, width = 6, height = 4, dpi = 150)
      report(glue("{msg_prefix} -> {p_path}"))

      invisible(tune_path)

    }, data_env = data_env)

  return(tune_paths)

}

# Cross-validate
# ~ 4h00 (3h15 - 5h45 per year)
resample_m2 <- function(year, months = study_months(year), ncores = 1L, force = FALSE,
                        verbose = FALSE) {

  # Ensure all months are in study period (exclude Jan + Feb 2000 b/c MAIAC AOD begins 2000-02-26)
  if (!missing(months)) {

    check <- setdiff(months, study_months(year))
    if (length(check) > 0) {
      stop(glue("Months [{paste(check, collapse = ' ')}] not in study period for {year}"))
    }

  }

  # Skip if already processed
  cv_paths <- lubridate::make_date(year, months, 1) %>%
              purrr::set_names() %>%
              purrr::map_chr(function(day) {
                here(glue("work/models/{year}/m2_{format(day, '%Y-%m')}_cv.fst"))
              })
  if (all_exist(cv_paths, force = force)) return(as.character(cv_paths))

  report(glue("[resample_m2] {year}: cross-validate m2 [MAIAC AOD ~ RF(EAC4/MERRA2 AOD)]"))

  # Create a new environment to hold the data
  # This avoids loading data unless it is needed by an iteration of the loop over months
  data_env = new.env()

  # Cross-validate by month
  # Training time increases ~ exponentially with number of rows
  path_missing(cv_paths, force = force) %>%
    purrr::iwalk(function(cv_path, day, data_env) {

      day %<>% lubridate::ymd()
      ym <- format(day, "%Y-%m")

      # Raise if the model has not yet been tuned
      tune_path <- str_replace(cv_path, "_cv.fst", "_tune.rds")
      if (!file.exists(tune_path)) {
        stop(glue("Tune result not found: {tune_path}"))
      }

      msg_prefix <- glue("[resample_m2] {ym}:")

      # Load db2_cc and define the task, learner, and resampling
      report(glue("{msg_prefix} assembling m2 components"), verbose)
      parts <- here(glue("work/db/{year}/db2_{year}_cc.fst")) %>%
               prep_m2(data_env = data_env, db2_cc_path = ., day = day, ncores = ncores)

      # Load the tune result and update the learner with the optimal mtry
      # Also estimate feature importance by permutation (doubles time but may be interesting)
      best <- readRDS(tune_path)$x
      parts$learner %<>% mlr::setHyperPars(importance = "permutation", par.vals = best)

      # Define a measure that reports the test set size as a fraction of the total dataset
      test_frac <- mlr::makeMeasure(
                     id = "test_frac",
                     minimize = FALSE,
                     properties = c("regr", "req.pred", "req.task"),
                     fun = function(task, model, pred, feats, extra.args) {
                             round(nrow(as.data.frame(pred)) / getTaskSize(task), 3)
                           }
                   )

      # Cross-validate the model
      report(glue("{msg_prefix} cross-validating"), verbose)
      set.seed(100)
      m2 <- mlr::resample(
              learner = parts$learner,
              task = parts$task,
              resampling = parts$resampling,
              measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_frac, mlr::timetrain),
              extract = mlr::getFeatureImportance,
              show.info = verbose
            )
      set.seed(Sys.time())

      # Join the CV predictions to some useful columns of data
      cv <- data_env$db2_cc[parts$data_idx,
                            .(date, sinu_id, sinu_x, sinu_y, time, maiac_n, cv_block, aod_047)]
      cv[m2$pred$data$id, `:=`(pred_m2 = m2$pred$data$response, cv_fold = m2$pred$data$iter)]

      # Save the CV predictions
      save_safely(cv, fun = write_fst, path = cv_path, compress = 100)
      report(glue("{msg_prefix} -> {cv_path}"))

      # Save each fold's feature importance
      # * Join per-fold performance metrics
      feat_imp <- purrr::map(m2$extract, "res") %>%
                  purrr::imap_dfr(~ cbind(.x, fold = .y)) %>%
                  dplyr::right_join(m2$measures.test, by = c(fold = "iter")) %>%
                  as.data.table()
      feat_imp_path <- str_replace(cv_path, "_cv.fst", "_cv_feats.fst")
      save_safely(feat_imp, fun = write_fst, path = feat_imp_path, compress = 100)
      report(glue("{msg_prefix} -> {feat_imp_path}"))

      # Plot each fold's feature importance
      # * Order bars (1 per fold) by MAE ascending
      # * Order variables by mean importance descending
      var_order <- dplyr::group_by(feat_imp, variable) %>%
                   dplyr::summarise(importance = mean(importance), .groups = "drop_last") %>%
                   dplyr::arrange(importance) %>%
                   dplyr::pull(variable)
      feat_imp$variable %<>% factor(levels = var_order)
      p <- ggplot(feat_imp, aes(x = importance, y = variable)) +
             geom_col(aes(fill = as.factor(round(rsq, 3))), position = "dodge") +
             scale_fill_brewer(type = "seq") +
             labs(fill = "Fold R2", title = glue("m2 {ym} cv"))
      p_path <- here(glue("work/review/{year}/m2_{ym}_cv_feats.png"))
      save_safely(p, fun = ggsave, path = p_path, width = 8, height = 5, dpi = 150)
      report(glue("{msg_prefix} -> {p_path}"))

      invisible(cv_path)

    }, data_env = data_env)

  # Load monthly cross-validation results and report performance
  cv <- purrr::map(cv_paths, function(p) {
          read_fst(p, as.data.table = TRUE) %>%
            .[, assess_preds(.SD, "aod_047", "pred_m2", "sinu_id"), keyby = .(month(date))]
        })
  cv %<>% rbindlist() %>%
          setkey("month")

  report(glue("[resample_m2] {year}: monthly CV"), verbose)
  report(summarize_perf(cv), verbose)
  # [resample_m2] 2012 monthly CV
  # month      r2     mae    rmse res.min res.max pred.min pred.max obs.min obs.max   obs.n r2.space r2.time         i  slope
  #     1  0.5695 0.02541 0.03497 -0.8459  0.3654  0.02209   0.5024       0   0.989 3890698   0.5739  0.6155 -0.007265 0.9612
  #     2  0.7100 0.03051 0.04182 -0.7804  0.3924  0.02705   0.7665       0   0.998 4758953   0.6074  0.7606 -0.009263 0.9845
  #     3  0.8239 0.02832 0.04086 -0.8423  0.5088  0.01900   0.8773       0   0.998 9664592   0.6980  0.8477 -0.003088 0.9796
  #     4  0.8037 0.03872 0.05614 -0.8315  0.3995  0.01900   0.9950       0   0.999 2064746   0.7300  0.8677 -0.010246 0.9408
  #     5  0.8263 0.03446 0.05061 -0.6541  0.4823  0.01963   0.9551       0   0.999 4740659   0.7028  0.8555 -0.011459 0.9923
  #     6  0.8187 0.03493 0.05084 -0.6805  0.4279  0.01922   0.9830       0   0.999 3516072   0.8111  0.8554 -0.003299 0.9397
  #     7  0.8082 0.02724 0.04002 -0.7051  0.3271  0.01907   0.7896       0   0.992 5280897   0.6560  0.8436 -0.002609 0.9471
  #     8  0.8571 0.02727 0.04213 -0.8100  0.4096  0.01924   0.9271       0   0.999 6707629   0.7393  0.8789 -0.003796 0.9594
  #     9  0.8613 0.02864 0.04125 -0.7590  0.5513  0.01900   0.9916       0   0.999 4982853   0.7786  0.8895 -0.005733 0.9592
  #    10  0.6944 0.02502 0.03534 -0.8130  0.2814  0.02058   0.5239       0   0.991 3608141   0.7140  0.7074 -0.006942 0.9738
  #    11  0.4982 0.02532 0.03450 -0.8495  0.2592  0.02373   0.5103       0   0.999 3468531   0.5762  0.5491 -0.004672 0.9290
  #    12  0.4754 0.02454 0.03377 -0.8651  0.2486  0.02742   0.3606       0   0.966 2545932   0.5561  0.5188 -0.003546 0.9235

  report(glue("[resample_m2] {year}: annual CV"))
  cv[, lapply(.SD, mean)] %>%
    .[, -"month"] %>%
    summarize_perf() %>%
    report()
  # [resample_m2] 2012 annual CV
  #     r2    mae    rmse res.min res.max pred.min pred.max obs.min obs.max   obs.n r2.space r2.time         i  slope
  # 0.7289 0.0292 0.04186 -0.7864  0.3878  0.02125   0.7652       0   0.994 4602475   0.6786  0.7658 -0.005993 0.9575

  return(cv_paths)

}

# Fit final model
# ~ 1h30 (1h00 - 2h00 per year)
fit_m2 <- function(year, months = study_months(year), ncores = 1L, force = FALSE, verbose = FALSE) {

  # Ensure all months are in study period (exclude Jan + Feb 2000 b/c MAIAC AOD begins 2000-02-26)
  if (!missing(months)) {

    check <- setdiff(months, study_months(year))
    if (length(check) > 0) {
      stop(glue("Months [{paste(check, collapse = ' ')}] not in study period for {year}"))
    }

  }

  # Skip if already processed
  m2_paths <- lubridate::make_date(year, months, 1) %>%
              purrr::set_names() %>%
              purrr::map_chr(function(day) {
                here(glue("work/models/{year}/m2_{format(day, '%Y-%m')}.rds"))
              })
  if (all_exist(m2_paths, force = force)) return(as.character(m2_paths))

  report(glue("[fit_m2] {year}: fit m2 [MAIAC AOD ~ RF(EAC4/MERRA2 AOD)]"))

  # Create a new environment to hold the data
  # This avoids loading data unless it is needed by an iteration of the loop over months
  data_env = new.env()

  # Fit by month
  # Training time increases ~ exponentially with number of rows
  path_missing(m2_paths, force = force) %>%
    purrr::iwalk(function(m2_path, day, data_env) {

      day %<>% lubridate::ymd()
      ym <- format(day, "%Y-%m")

      # Fail if the model has not yet been tuned
      tune_path <- str_replace(m2_path, ".rds", "_tune.rds")
      if (!file.exists(tune_path)) {
        stop(glue("Tune result not found: {m2_tune_path}"))
      }

      msg_prefix <- glue("[fit_m2] {ym}:")

      # Load db2_cc data for the month and define the task, learner, and resampling
      report(glue("{msg_prefix} assembling m2 components"), verbose)
      parts <- here(glue("work/db/{year}/db2_{year}_cc.fst")) %>%
               prep_m2(data_env = data_env, db2_cc_path = ., day = day, ncores = ncores)

      # Load the tune result and update the learner with the optimal mtry
      # Also estimate feature importance by permutation (doubles time but may be interesting)
      best <- readRDS(tune_path)$x
      parts$learner %<>% mlr::setHyperPars(importance = "permutation", par.vals = best)

      # Train the model on the entire dataset
      report(glue("{msg_prefix} fitting final model"), verbose)
      set.seed(100)
      m2 <- mlr::train(learner = parts$learner, task = parts$task)
      set.seed(Sys.time())

      # Save the final model
      save_safely(m2, fun = saveRDS, path = m2_path)
      report(glue("{msg_prefix} -> {m2_path}"))

      # Plot final feature importance
      p <- mlr::getFeatureImportance(m2)$res %>%
           ggplot(aes(y = reorder(variable, importance), x = importance)) +
             geom_col() +
             labs(y = "Feature", title = glue("m2 {ym} final"))
      p_path <- here(glue("work/review/{year}/m2_{ym}_feats.png"))
      save_safely(p, fun = ggsave, path = p_path, width = 7, height = 4, dpi = 150)
      report(glue("{msg_prefix} -> {p_path}"))

      invisible(m2_path)

    }, data_env = data_env)

  return(m2_paths)

}

# Gapfill missing MAIAC AOD
# ~ 1h20 (0h50 - 2h45 per year)
predict_p2 <- function(year, months = study_months(year), ncores = 1L, force = FALSE,
                       verbose = FALSE) {

  # Ensure all months are in study period (exclude Jan + Feb 2000 b/c MAIAC AOD begins 2000-02-26)
  if (!missing(months)) {

    check <- setdiff(months, study_months(year))
    if (length(check) > 0) {
      stop(glue("Months [{paste(check, collapse = ' ')}] not in study period for {year}"))
    }

  }

  # Skip if already processed
  p2_paths <- lubridate::make_date(year, months, 1) %>%
              purrr::set_names() %>%
              purrr::map_chr(function(day) {
                here(glue("work/preds/{year}/p2_{format(day, '%Y-%m')}.fst"))
              })
  if (all_exist(p2_paths, force = force)) return(as.character(p2_paths))

  report(glue("[predict_p2] {year}: impute missing MAIAC AOD using m2"))

  # Predict by month
  path_missing(p2_paths, force = force) %>%
    purrr::iwalk(function(p2_path, day) {

      day %<>% lubridate::ymd()
      ym <- format(day, "%Y-%m")

      msg_prefix <- glue("[predict_p2] {ym}:")

      # Load m2 and db2
      report(glue("{msg_prefix} loading m2 and db2"), verbose)
      m2 <- here(glue("work/models/{year}/m2_{ym}.rds")) %>%
            readRDS()
      db2 <- here(glue("work/db/{year}/db2_{ym}.fst")) %>%
             read_fst(as.data.table = TRUE)

      # Predict
      report(glue("{msg_prefix} predicting"), verbose)
      setDF(db2)
      preds <- predict(m2, newdata = db2)

      # Add the predictions to db2 and drop unneeded columns
      p2 <- setDT(db2) %>%
            .[, .(date, sinu_id, sinu_x, sinu_y, aod_047)]
      rm(db2)
      p2[, pred_m2 := round(getPredictionResponse(preds), 3)]
      setkeyv(p2, c("date", "sinu_id"))

      # Combine MAIAC AOD and predicted AOD into a single column
      # * 'aod_imp' identifies values that have been gapfilled vs. observed
      p2[, aod_imp := FALSE]
      p2[is.na(aod_047), `:=`(aod_imp = TRUE, aod_047 = pred_m2)]
      p2[, pred_m2 := NULL]
      setnames(p2, "aod_047", "aod")
      names(p2) %>% paste(collapse = " ")
      # date sinu_id sinu_x sinu_y time maiac_n aod aod_imp

      # Save
      save_safely(p2, fun = write_fst, path = p2_path, compress = 100)
      report(glue("{msg_prefix} -> {p2_path}"))

      # Summarize per-cell AOD mean, sd, and missingness, plus observed & imputed for 4 days
      report(glue("{msg_prefix} summarizing"), verbose)
      smry <- p2[, .(
                     sinu_x = first(sinu_x),
                     sinu_y = first(sinu_y),
                     aod.obs_frac = (.N - sum(aod_imp)) / .N,
                     aod.mean = mean(aod),
                     aod.min = min(aod),
                     aod.max = max(aod)
                   ), keyby = .(sinu_id)]
      days <- p2[date %in% (day + c(0, 7, 14, 21)), ] %>%
              .[aod_imp == FALSE, maiac := aod] %>%
              .[, .(date, sinu_id, maiac, imputed = aod)] %>%
              dcast(sinu_id ~ date, value.var = c("maiac", "imputed"))
      dt_left_join(smry, days)
      rm(days)

      # Save the summary as a raster
      smry_path <- str_replace(p2_path, "\\.fst$", "_smry.tif")
      save_safely(sinu_to_raster(smry), fun = save_geotiff, path = smry_path, datatype = "FLT8S")
      report(glue("{msg_prefix} -> {smry_path}"))

      # Save a plot of the summary
      report(glue("{msg_prefix} plotting summary"), verbose)
      smry %<>% .[, !"sinu_id"] %>%
                melt(id.vars = c("sinu_x", "sinu_y"), variable.name = "lyr")
      roi <- st_read("work/geo/roi.gpkg", quiet = TRUE) %>%
             st_transform(crs = MODIS_SINU)
      plot_fun <- function(x, legend_hgt) {
        ggplot(x, aes(x = sinu_x, y = sinu_y, fill = value)) +
          geom_tile() +
          geom_sf(data = roi, inherit.aes = FALSE, fill = NA) +
          facet_wrap(vars(lyr), ncol = 1) +
          scale_fill_viridis_c(option = "inferno", na.value = NA, name = NULL) +
          coord_sf(datum = NULL, label_axes = "----") +
          theme_bw() +
          theme(axis.title = element_blank(), panel.grid = element_blank(),
                legend.key.height = grid::unit(legend_hgt, "npc"))
      }
      p <- cowplot::plot_grid(
             plot_fun(smry[lyr == "aod.obs_frac", ], legend_hgt = 0.08),
             plot_fun(smry[lyr == "aod.mean", ],     legend_hgt = 0.08),
             plot_fun(smry[lyr == "aod.min", ],      legend_hgt = 0.08),
             plot_fun(smry[lyr == "aod.max", ],      legend_hgt = 0.08),
             plot_fun(smry[lyr %like% "-01$", ], legend_hgt = 0.18),
             plot_fun(smry[lyr %like% "-08$", ], legend_hgt = 0.18),
             plot_fun(smry[lyr %like% "-15$", ], legend_hgt = 0.18),
             plot_fun(smry[lyr %like% "-22$", ], legend_hgt = 0.18),
             nrow = 2,
             ncol = 4,
             rel_heights = c(1, 2)
           )
      plot_path <- basename(p2_path) %>%
                   str_replace("\\.fst$", "_smry.png") %>%
                   glue("work/review/{year}/", .) %>%
                   here()
      save_safely(p, fun = ggsave, path = plot_path, width = 15, height = 9, dpi = 100)
      report(glue("{msg_prefix} -> {plot_path}"))

      invisible(p2_path)

    })

  return(p2_paths)

}
