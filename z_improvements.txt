Stage 1 (impute PM2.5 at PM10 stations)
* Incorporate meteo variables (e.g. temperature, humidity, wind)

Stage 2 (gapfill MAIAC AOD)
* Incorporate a MAIAC aod quality score
* Drop MAIAC AOD for pixels with > 50 % water?
* Keep all MAIAC observations (rather than calculating daily mean)

Stage 3 (predict PM from gapfilled AOD + covariates)
* Add MAIAC AOD at 550 nm
* Add INS 2007 annual spatial emissions inventory
* Explore spatial and temporal lagging of station PM
  - e.g. via IDW for same day and previous days, see Di et al. 2019)
    - what is mean distance between monitors?
    - how cross-validate if using spatially lagged data?
* Add SD of elevation (and other terrain indices?)
* Add SD of wind
* Add SD of Ta
* Add SD of PBL height
* Add Upwind cell AOD
* Use Meteo vars at overpass time (not mean for day)
* Add Dust days
* Add monitored NO2 (spatially lagged)
* Add OMI NO2
* Add MACC-II and CAMS PM reanalysis / forecasts
* Add x-km radius aggregation of spatial predictors to capture area effects
  - what radius?
* Temporally interpolate NDVI
* Add CEIP gridded 10 km emissions https://www.ceip.at/ms/ceip_home1/ceip_home/new_emep-grid/01_grid_data/index.html
