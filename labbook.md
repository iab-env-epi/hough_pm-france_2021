# P063.FR.PM

PM10 and PM2.5 air pollution model for continental France

## Changelog

## Datasets

Spatiotemporal data
-------------------

[ ] PM monitor data
    [ ] France
    [ ] Switzerland
    [ ] Germany
    [ ] Luxembourg
    [ ] Belgium
[X] AOD - MAIAC
[ ] AOD - ECMWF
    [ ] CAMS
    [ ] MACC
    [ ] MACC-II
[ ] Meteorology
    [ ] Ta
    [ ] Pressure
    [ ] Precipitation
    [ ] Humidity
    [ ] Wind speed
    [ ] Wind direction
    [ ] PBL height
[ ] NDVI
    [ ] MODIS 1 km
    [ ] MODIS 250 m
    [ ] Landsat 100 m
[ ] Dust days
[ ] Elevation
[ ] Land cover
[ ] Population
[ ] Roads
[ ] Light at night
[ ] Emissions sources
    [ ] Point
    [ ] Areal
[ ] Water bodies


Spatial data
------------

[ ] Elevation
[ ] Land cover
[ ] Population
[ ] Impervious cover
[ ] Road density
[ ] Road distance
[ ] Light at night
[ ] Point emissions
[ ] Airport distance
[ ] Port distance
[ ] Sea distance
[ ] Lake distance



Variables
---------

Day
Day of week
Month
Latitude
Longitude
Geoclimatic zone
Admin region

AOD 470 nm
AOD 550 nm
Ta
Pressure
Humidity
Wind u
Wind v
PBL (0:00 / 6:00 / 12:00 / 18:00)
Precipitation

NDVI
Light at night

Point source PM10
Areal source PM10
Desert dust advection

Elevation
Population
Impervious surface area

Street density
Road (major + minor) density
Highway density
Rail density

Distance from sea
Distance from emission points
Distance from highways
Distance from major roads
Distance from airports
Distance from rail

% low development
% high development
% arable land
% crops
% pasture
% deciduous
% evergreen
% agricultural
% shrub






Emissions inventories
  CEIP: http://www.ceip.at/ms/ceip_home1/ceip_home/webdab_emepdatabase/webdab_usersguide/
  EDGAR: http://edgar.jrc.ec.europa.eu/overview.php?v=432_AP



CAMS (Copernicus Atmosphere Monitoring Service) products
--------------------------------------------------------

* MACC (Monitoring Atmospheric Composition and Climate)
  - Obsolete; superseded by MACC II
* MACC II (Monitoring Atmospheric Composition and Climate Interim Implementation)
  - Obsolete? Superseded by CAMS reanalysis?
  - Global / 2003 - 2012 / 0.75 x 0.75 (~80 km)
  - AOD / meteo
  - Documentation: https://confluence.ecmwf.int/pages/viewpage.action?pageId=81010554
  - Data: https://apps.ecmwf.int/datasets/data/macc-reanalysis/levtype=sfc/
* MACC III
  - Obsolete; superseded by CAMS reanalysis
* CAMSiRA (CAMS interim)
  - Obsolete; superseded by CAMS reanalysis
* CAMS global reanalysis
  - Global / 2003 - 2016 / 0.75 x 0.75 (~80 km)
  - AOD / PM10 / PM2.5 / NOx / SO2 / ozone / meteo
  - Documentation: https://confluence.ecmwf.int/display/CKB/CAMS+Reanalysis+data+documentation#CAMSReanalysisdatadocumentation-Introduction
  - Data: https://apps.ecmwf.int/data-catalogues/cams-reanalysis/?class=mc&expver=eac4
* CAMS regional reanalysis
  - Europe / 2010 - 2018 / 0.1 x 0.1 (~10 km)
  - PM10 / PM2.5 / NOx / SO2 / ozone
  - Documentation: 
  - Data: http://www.regional.atmosphere.copernicus.eu/index.php
* CAMS regional forecast archives
  - Europe / Oct 2015 - present / 0.1 x 0.1 (~10 km)
  - PM10 / PM2.5 / NOx / SO2 / ozone
  - Documentation: https://atmosphere.copernicus.eu/documentation-regional-systems
  - Data: http://www.regional.atmosphere.copernicus.eu/index.php
* CAMS global forecast archives
  - Global / Jul 2012 - present / 0.4 x 0.4 (~40 km)
  - AOD / PM10 / PM2.5 / NOx / SO2 / ozone / meteo
  - Documentation: https://atmosphere.copernicus.eu/global-products
  - Data: https://apps.ecmwf.int/datasets/data/cams-nrealtime/levtype=sfc/

ECMWF meteorological products
-----------------------------

* UERRA (Uncertainties in Ensembles of Regional ReAnalysis)
  - Europe / 1961 - present / 0.1 x 0.1 (~11 km UERRA-HARMONIE; ~5.5 km MESCAN-SURFEX)
  - T / wind / humidity / pressure (UERRA-HARMONIE = 3dVar assimilation system)
  - T / wind / humidity / precipitation (MESCAN-SURFEX = surface analysis system)
  - Documentation: https://confluence.ecmwf.int/display/UER
  - Data: https://apps.ecmwf.int/datasets/data/uerra/levtype=sfc/stream=oper/type=an/ (UERRA-HARMONIE)
       or https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-uerra-europe-single-levels (MESCAN-SURFEX)
* ERA5
  - Global / 1979 - present / 0.25 x 0.25 (~28 km)
  - T / wind / humidity / pressure / PBL
  - Documentation: https://confluence.ecmwf.int/display/CKB/ERA5+data+documentation
  - Data: https://apps.ecmwf.int/data-catalogues/era5/?class=ea
